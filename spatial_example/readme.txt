Example case for spatial stability of FENE-P planar jet with thick shear layer
(Re=100, beta=0.7, N^-1= 0.555, S=1, L=100, see RZ15, section IIIB)

To run code, simply run the executable, shoot2w_final at the command line:
$ ./shoot2w_final

To create the executable with gfortran in the main directory (after compiling the files in netlib/odepack/):
$ make -f Makefile shoot2w_final

files:

shoot2w_final: Executable

input2pw.in: Input file specifying fluid, flow, and numerical parameters

alpha1.out: Primary output file. First column is real part of frequency, 2nd and 3rd
            columns are real and imaginary parts of wavenumber, 4th column is 1 if
            eigenvalue was successfully found, 5th column is a error paramter
            returned by minimization code. See shoot2w_final.F90 for further details.

alphar.out, alphai.out: Real and imaginary parts of eigenvalue

mean.out: Columns correspond to z,U, dU/dz, d2U/dz2, d3U/dz3

polymer_mean.out: Columns correspond to z, A11 , dA11/dz, d2A11/dz2,  A12 , dA12/dz,
                  d2A12/dz2,  A22 , dA22/dz, d2A22/dz2,

rI.out, rO.out: grid points where orthogonalization is carried out
