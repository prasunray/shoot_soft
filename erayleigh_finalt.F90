! axisymmetric modes, axisymmetric jet
!  shoot.F90
! 7/25/11: modified version of shoot2w.F90, search for absolute instabilities following
!		   Monkewitz (1988).
! 2/26/09: modified version of shoot2.F90 -- Cartesian
! 4/27/06: modified version of shoot.F90 -- matches solutions at rm  
!
! 

!---------------------------------------------------------------------------------
module variables
implicit none
save
integer :: Nwr,Nwi,Nvary
integer, parameter :: Nlm=6
real*8 :: r0,rf,rm,rel_tol,err_w0,err_k0,scratch,alpha_relax
real*8 :: E,St,wr1,wr2,wi1,wi2 
real*8 ::   kkf(3),kkg(3),kk0g,kktemp(3),kmm(3),aaf(3)
complex*16 :: ww,aa,bb,kk,kk2,wwf(3),wwg(3),ww0g,cc1g,cc2g,wwtemp(3)
complex*16, allocatable, dimension(:,:) :: dke
real*8, allocatable, dimension(:) :: kk0,aaff
complex*16, allocatable, dimension(:) :: ww0,cc1,cc2,wwff,cc1ff,cc2ff,uu,vv,tt,pp,vvy,vvyy 
complex*16, allocatable, dimension(:) :: a11v,a12v,a22v,duu,d2uu,d2vv,dpp
real*8 :: ue,theta,U,dU,d2U,d3U
integer :: vary_E,vary_ue,vary_theta,vary_lg,vary_Sc
real*8 :: uei,thetai,Ei,uef,thetaf,Ef,lgi,lgf,Sci,Scf
complex*16, allocatable, dimension(:,:,:) :: P_orthoI,P_orthoO
integer, allocatable, dimension(:) :: j_orthoI,j_orthoO
integer :: flag_orthoI,flag_orthoO,flag_symmetry,mean_type
real*8 :: lg,sg, Sc !Input parameters, lambda, Greek letter I don't know, Schmidt number.
integer :: flag_display
real*8 :: lL_FENE,lL_FENEi,fI2,dffI
end module variables
!---------------------------------------------------------------------------------
module polymer_variables
implicit none
save
integer :: model_type
complex*16 :: phi,psi,tau,A11,A12,A22,phip,psip,taup,A11p,A12p,A22p
complex*16 :: G221,G222,G223,G221p,G222p,G223p
complex*16 :: c131,c132,c133,c131p,c132p,c133p
complex*16 :: c31,c32,c31p,c32p
complex*16 :: chi,chip,F22,F22p
real*8 :: a11m,a12m,a22m,Da11m,Da12m,Da22m,D2a11m,D2a12m,D2a22m !mean polymeric stress and derivatives
complex*16 :: L1p
complex*16 :: M1,M2,M3,M1p,M2p,M3p
complex*16 :: N1,N2,N3,N1p,N2p,N3p
complex*16 :: R1,R2,R3,R4
complex*16 :: S1,S2,S3,S4
complex*16 :: P1,P2,P3,B1,B2,P1p,P2p,P3p,B1p,B2p
complex*16 :: c231,c232,c233,c231p,c232p,c233p
complex*16 :: c1231,c1232,c1233,c1231p,c1232p,c1233p
real*8 :: CM11,CM12,CM22,DCM11,DCM12,DCM22,D2CM11,D2CM12,D2CM22,CMKK !FENE-P: mean conformation tensor & derivatives
real*8 :: L_FENE,L_FENEi,L_FENEf !FENE-P: maximum extensibility
integer :: vary_L_FENE
end module polymer_variables

!---------------------------------------------------------------------------------

	  program shoot
	  use grid
	  use parameters
	  use variables
	  use hybrd_variables
	  use dlsode_variables
	  use spline_variables
	  use lmdif_variables
	  use polymer_variables

	  
	  implicit none
	  integer :: a1,i1,j1,k1,l1,count
	  integer, parameter :: k1max=1000
		external OS_integrate,least_squares
 

		open(unit=9,file='inputert.in')
		read(9,*) model_type		
		
		read(9,*) kkf(1) !initial wavenumbers (real)
		read(9,*) kkf(2)
		read(9,*) kkf(3)
		
		read(9,*) wwf(1) ! frequency guesses (complex)
		read(9,*) wwf(2)
		read(9,*) wwf(3)

		read(9,*) Ei
		read(9,*) thetai
		read(9,*) Uei
		read(9,*) NI
		read(9,*) NO
		read(9,*) r0
		read(9,*) rm
		read(9,*) rf
		read(9,*) read_grid
		read(9,*) flag_symmetry
		read(9,*) mean_type
!	if (mean_type==1) then
		read(9,*) Nspline
!	end if
		read(9,*) flag_display
		read(9,*) rel_tol
		read(9,*) vary_E,Ef
		read(9,*) vary_theta,thetaf
		read(9,*) vary_ue,uef
		read(9,*) vary_lg,lgf
		read(9,*) vary_Sc,Scf
		read(9,*) Nvary
		read(9,*) alpha_relax
		if (model_type==3) then
			read(9,*) L_FENEi
			read(9,*) vary_L_FENE, L_FENEf
		end if
		close(9)
		allocate(infoA(3,1),fvecA(3,1))
!		allocate(wwf(Nwr,Nwi),aaf(Nwr,Nwi),infoA(Nwr,Nwi),fvecA(Nwr,Nwi))
!		do i1=1,Nwr
!			do j1=1,Nwi
!				wwf(i1,j1)=cmplx((wr1+real(i1-1)*(wr2-wr1)/real(Nwr-1+1.0e-16)),(wi1+real(j1-1)*(wi2-wi1)/real(Nwi-1+1.0e-16)))
!			end do
!		end do
		if (Nvary < 1) Nvary=1
		print *, 'Ef,thetaf,uef,Nvary:',Ef,thetaf,uef,Nvary
		allocate(wwff(Nvary),aaff(Nvary),cc1ff(Nvary),cc2ff(Nvary))
				
	    if (mean_type==1) then
			E=Ei/1.7208
			wwf=wwf/1.7208
			aa_guess=aa_guess/1.7208
		end if

		print *, 'E=',Ei
				bb=0.
	
		tol=1.e-8
		err_w0=10.0
		print *, 'set parameters'
		
		!ALLOCATE VARIABLES
		allocate(ZI(NI),ZO(NO))
	  
	  !make grid
	 
	  call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
		open(unit=9,file='rI.out')
 			do i1=1,NI
		!write(9,*) Z(N+1-i1)
			write(9,*) ZI(i1)
			end do
		close(9)
		
		open(unit=9,file='rO.out')
 			do i1=1,NO
		!write(9,*) Z(N+1-i1)
			write(9,*) ZO(i1)
			end do
		close(9)
	   print *, 'generated grid'
	 
	 
		if (mean_type==1) then
	!intialize spline for Blasius mean flow
			allocate(zspline(Nspline),yspu(Nspline,2),uspline(Nspline,2))
			do i1=1,Nspline
				zspline(i1) = r0 + real(i1-1)/real(Nspline-1)*(rf-r0)
				call calculate_velocity(zspline(i1),uspline(i1,1),dU,uspline(i1,2))
			end do
	call spline(zspline,uspline(:,1),Nspline,0.0,1.0e31,yspu(:,1))
	call spline(zspline,uspline(:,2),Nspline,0.0,1.0e31,yspu(:,2))
	end if
		E=Ei
		theta=thetai
!		ue=1.0/uei
		ue = uei
		lg = lgi
		Sc = Sci
		L_FENE = L_FENEi

	!output mean flow
	open(unit=9,file='mean.out')
	open(unit=10,file='polymer_mean.out')
	do i1=1,NI
			call mean_flow(mean_type,ZI(i1),thetai,Uei,rf,U,dU,d2U,d3U)
			call polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)
		write(9,'(5E28.16)') zI(i1),U,dU,d2U,d3U
		write(10,'(10E28.16)') zI(i1),a11m,da11m,d2a11m,a12m,da12m,d2a12m,a22m,da22m,d2a22m
	end do
	do i1=NO-1,1,-1
		call mean_flow(mean_type,ZO(i1),thetai,Uei,rf,U,dU,d2U,d3U)
		call polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)

		write(9,'(5E28.16)') zO(i1),U,dU,d2U,d3U
		write(10,'(10E28.16)') zO(i1),a11m,da11m,d2a11m,a12m,da12m,d2a12m,a22m,da22m,d2a22m

	end do
	close(9)
	close(10)
	

	!call minimizing routine with (fcn=OS_integrate)
		allocate(P_orthoI(Neq/4,Neq/4,NI),j_orthoI(NI))
		P_orthoI=0.0
		j_orthoI=0
		flag_orthoI=1

		allocate(P_orthoO(Neq/4,Neq/4,NO),j_orthoO(NO))
		P_orthoO=0.0
		j_orthoO=0
		flag_orthoO=1
		allocate(kk0(k1max),ww0(k1max),cc1(k1max),cc2(k1max))
		
		do l1=1,Nvary !outer loop, external parameter

			!---vary external parameter			
			if (vary_E==1) E=Ei + real(l1-1)/real(Nvary-1)*(Ef-Ei)
			if (vary_ue==1) then
				ue=uei + real(l1-1)/real(Nvary-1)*(uef-uei)
!				ue=1./ue
			end if
			if (vary_theta==1) theta=thetai + real(l1-1)/real(Nvary-1)*(thetaf-thetai)
			if (vary_lg==1) lg=lgi + real(l1-1)/real(Nvary-1)*(lgf-lgi)
			if (vary_Sc==1) Sc=Sci + real(l1-1)/real(Nvary-1)*(Scf-Sci)
			if ((model_type==3) .AND. (vary_L_FENE==1)) L_FENE=L_FENEi + real(l1-1)/real(Nvary-1)*(L_FENEf-L_FENEi)
			!-------------------------------
				print *, '+++ l1,E,ue,theta,lg,Sc=',l1,E,ue,theta,lg,Sc

			!---update guess for eigenvalue calculations
			if (l1==2) then
!				print *, 'wwg:',wwg,wwff(l1-1),wwff(l1-2),wwg+wwff(l1-1)-wwff(l1-2)
				kkf=kkg 
				!kkf=kkg 
				call evaluate_w(cc1(k1),cc2(k1),kk0(k1),ww0(k1),kkf,wwtemp)
				wwf=wwtemp			
			elseif (l1>2) then !extrapolate guess
				wwf=wwg + wwff(l1-1)-wwff(l1-2)
				kkf=kkg + aaff(l1-1)-aaff(l1-2)	
!				call evaluate_k(cc1(k1),cc2(k1),ww0(k1),kk0(k1),wwf(1,1),wwf(2,1),kktemp)
!				kkf=kktemp		
			end if
			!--------

		!---loop to find err_w0<rel_tol
			k1=0
		do while (((err_w0>rel_tol) .OR. (k1<2)) .AND. (k1<k1max))
			k1=k1+1
	
			!------update guess for eigenvalue calculations				
			if (k1 ==2 ) then
		!		wwf=(alpha_relax*wwf+(1.0-alpha_relax)*ww0(k1-1))
				kkf = (alpha_relax*kkf + (1.0-alpha_relax)*kk0(k1-1))
				call evaluate_w(cc1(k1-1),cc2(k1-1),kk0(k1-1),ww0(k1-1),kkf,wwtemp)
				wwf=wwtemp
			elseif (k1>2) then
				ww0g = 2.0*ww0(k1-1)-ww0(k1-2)
				kk0g = 2.0*kk0(k1-1)-kk0(k1-2)
!				wwf=(alpha_relax*wwf+(1.0-alpha_relax)*ww0g)
				kkf = (alpha_relax*kkf + (1.0-alpha_relax)*kk0g)
				call evaluate_w(cc1(k1-1),cc2(k1-1),kk0(k1-1),ww0(k1-1),kkf,wwtemp)
				wwf=wwtemp
				
			end if
			!-------
		
			!-----innermost loop, compute three eigenvalues, nonlinear least squares fit for w0,k0
			print *, '--- k1,err_w0=',k1,err_w0
			do i1=1,3 !frequencies
					aa=kkf(i1)
					aa_guess(1)=real(wwf(i1))
					aa_guess(2)=aimag(wwf(i1))
			

					print *, 'i1,aa,ww_guess=',i1,aa,cmplx(aa_guess(1),aa_guess(2))
					P_orthoI=0.0
					j_orthoI=0
					flag_orthoI=1

					P_orthoO=0.0
					j_orthoO=0
					flag_orthoO=1

				
					call hybrd1(OS_integrate,2,aa_guess,fvec,tol,info,wa,lwa)
	
					print *, 'computed eigenvalue, info=',info
					print *, 'fvec=',sqrt(fvec(1)**2+fvec(2)**2)
					print *, 'i1,St,aa=',i1,ww,cmplx(aa_guess(1),aa_guess(2))
					if ((info .ne. 1) .AND. (sqrt(fvec(1)**2+fvec(2)**2)>1.0e-8))then
						print *,'could not find eigenvalue'
						STOP
					end if
					infoA(i1,1)=info
					fvecA(i1,1)=sqrt(fvec(1)**2+fvec(2)**2)
					wwf(i1)=cmplx(aa_guess(1),aa_guess(2))
				end do !i1 -- innermost loop
			
		
		print *, 'wwf=',wwf
		print *, 'kkf=',kkf
!	if (abs(aaf(1,1)-aaf(1,2))<1e-8) STOP
!	if (abs(aaf(2,1)-aaf(2,2))<1e-8) STOP
	
	!solve nonlinear least squares problem to get estimate for (w0,k0)
		if ((k1==1) .AND. (l1==1)) then
			lm_x(1)=0.1
			lm_x(2)=-0.1
!			lm_x(4)=0.5
			lm_x(5)=-0.1
			
			lm_x(3)=real(sum(wwf/3.0))
			lm_x(4)=real(sum(kkf)/3.0)
			
			lm_x(6)=aimag(sum(wwf)/3.0)
!			lm_x(6)=0.0
!			lm_x(8)=aimag(0.25*sum(aaf))
		end if
		
		lmftol = 1.0e-16
		lmgtol = 1.0e-12 
		lmxtol = 1.0e-12
		lmmaxfev = 10000000
		lmepsfcn=1.0e-16
		lmfactor=100.0
		call lmdif(least_squares,Nlm,Nlm,lm_x,lmfvec,lmftol,lmxtol,lmgtol,lmmaxfev,lmepsfcn, &
                      lmdiag,lmmode,lmfactor,lmnprint,lminfo,lmnfev,lmfjac,lmldfjac, &
                      lmipvt,lmqtf,lmwa1,lmwa2,lmwa3,lmwa4)
	
	!computed new frequencies: w-ww0 = cc1*(k-kk0) + cc2*(k-kk0)**2
		cc1(k1) = lm_x(1) 
!		cc1(k1)=cmplx(lm_x(1),lm_x(4))
		cc2(k1)=cmplx(lm_x(2),lm_x(5))
		ww0(k1)=cmplx(lm_x(3),lm_x(6))
		kk0(k1)=lm_x(4) !cmplx(lm_x(4),lm_x(8))
	

		!use curve fit to get new k
		print *, '***info,fvec,w0,k0=',info,ww0(k1),kk0(k1),lmfvec
		err_w0 = abs(ww0(k1)-ww0(k1-1))
!		err_k0 = abs(kk0(k1)-k
		if (l1==1) then
			if (k1==1) then
				wwg=wwf
				kkg=kkf
			end if
		else
	 if ((err_w0>rel_tol*1000.0) .OR. (k1<=1) .OR. ((k1>1) .AND. (minval(real(kkg))<=1.0e-3).AND. (minval(real(aaf))>=1.0e-3)))then	
!		if ((err_w0>1.0e-3) .OR. (k1<=1)) then
				wwg=wwf
				kkg=kkf
			end if
		end if	
	


		print *, 'k1,err,tol=',k1,err_w0,rel_tol
		open(unit=21,file='kk0.out',position='append')
		open(unit=210,file='kkf.out',position='append')

		open(unit=22,file='ww0.out',position='append')
				open(unit=220,file='wwf.out',position='append')

		open(unit=23,file='cc.out',position='append')
		write(21,*) l1,real(kk0(k1))

		write(210,'(I4,3E28.16)') l1,kkf
		write(22,'(I4,6E28.16)') l1,real(ww0(k1)),aimag(ww0(k1)),err_w0,sum(abs(lmfvec))
		write(220,'(I4,6E28.16)') l1,real(wwf(1)),aimag(wwf(1)),real(wwf(2)),aimag(wwf(2)), &
					real(wwf(3)),aimag(wwf(3))
		write(23,'(I4,2E28.16)') l1,real(cc1(k1)),aimag(cc1(k1))
		close(21)
		close(210)
		close(220)
		close(22)
		close(23)


!write restart data
		
		if ((k1==1)) then			
			open(unit=240,file='restart.dat',position='append')
			write(240,*) 'l1=',l1
			write(240,*) 'E, theta=', E, theta,ue			
			write(240,*) 'lg,Sc,L=',lg,Sc,L_FENE			
			write(240,*) kkf(1)
			write(240,*) kkf(2)
			write(240,*) kkf(3)
			write(240,*) wwf(1)
			write(240,*) wwf(2)
			write(240,*) wwf(3)
			close(240)
		end if
	end do !while k1, loop for w0 convergence 
	
		
	cc1ff(l1)=cc1(k1)
	cc2ff(l1)=cc2(k1)
	wwff(l1)=ww0(k1)
	aaff(l1)=kk0(k1)

				P_orthoI=0.0
				j_orthoI=0
				flag_orthoI=1

				P_orthoO=0.0
				j_orthoO=0
				flag_orthoO=1
	aa=kk0(k1)
	aa_guess(1)=real(ww0(k1))
	aa_guess(2)=aimag(ww0(k1))
	call hybrd1(OS_integrate,2,aa_guess,fvec,tol,info,wa,lwa)
	print *, 'xxx check:',ww0(k1),kk0(k1),cmplx(aa_guess(1),aa_guess(2))

	if (l1==1) then
		open(unit=230,file='eig.out')
		open(unit=231,file='wwg.out')
		open(unit=232,file='kkg.out')
		
	else
		open(unit=230,file='eig.out',position='append')
		open(unit=231,file='wwg.out',position='append')
		open(unit=232,file='kkg.out',position='append')
		
	end if
		write(230,'(5E28.16)') real(wwff(l1)),aimag(wwff(l1)),aaff(l1)
		write(231,'(6E28.16)') real(wwg),aimag(wwg)
		write(232,'(3E28.16)') kkg		
	close(230)
	close(231)
	close(232)

	
	end do !l1, outermost loop
	

	
	      end program shoot
 !---------------------------------------------------------------------------------
subroutine output()
	use variables
	use grid
	implicit none
	integer :: i1,j1,k1,N
	complex*16, allocatable, dimension(:) :: pse_input
	N=NI+NO-1
	allocate(pse_input(3*N))
	open(unit=23,file='aa.out')
	write(23,*) real(aa),aimag(aa)
	close(23)
	
	
	open(unit=23,file='vectors.out')
	do i1=1,N
	write(23,('(8E28.16)')) real(uu(i1)),aimag(uu(i1)),real(vv(i1)),aimag(vv(i1)), &
		real(pp(i1)),aimag(pp(i1)),real(duu(i1)),aimag(duu(i1))
	end do
	close(23)
	
	
	open(unit=23,file='stress.out')
	do i1=1,N
	write(23,('(6E28.16)')) real(a11v(i1)),aimag(a11v(i1)),real(a12v(i1)),aimag(a12v(i1)), &
		real(a22v(i1)),aimag(a22v(i1))
	end do
	close(23)

	open(unit=23,file='dke.out')
	do i1=1,N
	write(23,('(7E28.16)')) dke(i1,:)
	end do
	close(23)
	
	
	open(unit=11,file='uu.out')
	open(unit=12,file='vv.out')
	open(unit=13,file='pp.out')
	

write(11,*) uu
write(12,*) vv
write(13,*) pp
close(11)
close(12)
close(13)






pse_input(1:N) = uu
pse_input(N+1:2*N) = vv
pse_input(2*N+1:3*N) = pp
open(unit=14,file='pse_input.out')
write(14,*) pse_input
close(14)



end subroutine output

!---------------------------------------------------------------------------------
	   
!---------------------------------------------------------------------------------
	   
	 subroutine OS_integrate(nx,x,fvec,iflag)
		use variables
		use parameters
		use dlsode_variables
		use grid
		implicit none
	    integer :: nx,iflag,i1,j1,k1,k10
		double precision :: x(nx),fvec(nx),za,zb
		complex*16 :: cc
		real*8 :: YSTART(NEQ,NEQ/4),angle(NEQ/4*(NEQ/4-1)/2)
		real*8, parameter :: h1=0.0001,hmin=1.e-14,eps=1.e-12
		complex*16 :: F1(NEQ/2,NEQ/4),F2(NEQ/2,NEQ/4),det_matrix(Neq/2,Neq/2), det,determinant,gamma
		real*8, parameter :: angle_threshold=5.0
		external Derivs,RKQS,jacobian
		
!		print *, 'enter OS_integrate'
	
		ww=cmplx(x(1),x(2))
		kk2 = aa*aa

	
		!integrate OS_equation twice with different initial conditions
		!integrate with subroutine Derivs 
	
		!*GENERATE INITIAL CONDITIONS
		YSTART=0.0
		call mean_flow(mean_type,r0,theta,Ue,rf,U,dU,d2U,d3U)
		gamma=ii*(aa*U-ww)

		call inner_conditions(NEQ,r0,aa,YSTART,flag_symmetry)
		if ((mean_type == 5) .and. (r0>0.0))   call inner_conditions(NEQ,r0,aa,YSTART,flag_symmetry+200)	
		    k10=1
             
        F1 =  cmplx(YSTART(1:NEQ/2,:),YSTART(NEQ/2+1:NEQ,:))		

		do k1=k10,NI-1
			do j1=1,NEQ/4
				ISTATE=1
				za=ZI(k1)
				zb=ZI(k1+1)
!!			print *, 'j1, YSTART=', j1, YSTART(:,j1)
!!			print *, 'za,zb=', ZI(k1),ZI(k1+1)
				rwork=0.0
				iwork=0
				iwork(6)=50000
				call DLSODE(Derivs,NEQ,YSTART(:,j1),za,zb,1,rtol,atol,1,istate,0,rwork,lrw,iwork,liw,jacobian,mf)
				do i1=1,NEQ/2
					F1(i1,j1)=cmplx(YSTART(i1,j1),YSTART(i1+NEQ/2,j1))
				end do
    
!!			print *, 'ISTATE=',ISTATE
!!			print *, 'YSTART=',YSTART(:,j1)
		end do
		
			
			YSTART(1:NEQ/2,:)=real(F1)
		    YSTART(NEQ/2+1:NEQ,:)=aimag(F1)
	end do		
		
		!*GENERATE OUTER CONDITIONS		

		if (mean_type==7)  then	
			call outer_conditions(NEQ,mean_type,rm,aa,F2)
			cc=ww/aa
			F2(2,1) = F2(2,1)*(cc**2)/(cc**2-16.0*E)
			!print *, 'outer conditions
		else
		call outer_conditions(NEQ,mean_type,rf,aa,F2)
!		print *, 'outer conditions:',F2
		k10=1
		YSTART(1:NEQ/2,:)=real(F2)
		YSTART(NEQ/2+1:NEQ,:)=aimag(F2)
		do k1=k10,NO-1
			do j1=1,NEQ/4
				ISTATE=1
				za=ZO(k1)
				zb=ZO(k1+1)
		!	print *, 'j1, YSTART=', j1, YSTART(:,j1)
		!	print *, 'za,zb=', Z(k1),Z(k1+1)
				rwork=0.0
				iwork=0
				iwork(6)=50000
			
				call DLSODE(Derivs,NEQ,YSTART(:,j1),za,zb,1,rtol,atol,1,istate,0,rwork,lrw,iwork,liw,jacobian,mf)

				do i1=1,NEQ/2
					F2(i1,j1)=cmplx(YSTART(i1,j1),YSTART(i1+NEQ/2,j1))
				end do
    
		!	print *, 'ISTATE=',ISTATE
		!	print *, 'YSTART=',YSTART(:,j1)
	
		end do
		
				
		
			YSTART(1:NEQ/2,:)=real(F2)
		    YSTART(NEQ/2+1:NEQ,:)=aimag(F2)
	end do		
		end if
	

		det = F2(1,1)*F1(2,1) - F2(2,1)*F1(1,1)
		fvec(1)=real(det)
		fvec(2)=aimag(det)
	
!		fvec(1) = x(1)**2-pi
!		fvec(2)=0.0
!!!	print *, 'aa,fvec=',aa,fvec
!!!	print *, 'ue,lg,sc,Re=',ue,lg,sc,Re
!!!	print *, 'det_matrix=',det_matrix

!!			print *, 'exit OS_integrate'
	!	STOP	
	end subroutine OS_integrate

!---------------------------------------------------------------------------------
subroutine Derivs(NEQ,x,Y,DYDX)
	use variables
	use polymer_variables
	use parameters
	implicit none
	integer :: NEQ,i1,j1
	double precision :: x, Y(*),DYDX(*)
	complex*16 :: MM(NEQ/2,NEQ/2),NNI(NEQ/2,NEQ/2),RHS(NEQ/2),f(NEQ/2),gamma,gg,ggp,gg1,gg1p,gg2,gg2p
	complex*16 :: R1g,R2g,R3g,R4g,S1g,S2g,S3g,S4g,invphi,RHSg(NEQ/2)
	real*8 :: MD(8)
!	print *, 'enter derivs'
	
	do i1=1,NEQ/2
		f(i1)=cmplx(Y(i1),Y(i1+NEQ/2))
	end do
	
	call mean_flow(mean_type,x,theta,Ue,rf,U,dU,d2U,d3U)
	
	
	
	RHS(1) = f(2)
	if (model_type==1) then
		gg = (U-ww/aa)**2 - 2.0*E*dU**2
		ggp = 2*dU*(U-ww/aa-2.0*E*d2U)
		RHS(2) = -ggp/gg*f(2) + aa*aa*f(1)
	elseif (model_type==3) then
		call  polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)
		gg = (U-ww/aa)**2 - 2.0*E*dU**2*fI2
		ggp = 2.0*dU*(U-ww/aa) - 4.0*E*dU*fI2*(d2U - dU*dffI)
		gg2 = 8.0*(dU**4)*(fI2**2)*(lL_FENE**2)*E
		gg2p = 32.0*(lL_FENE**2)*E*(dU**3)*(fI2**2)*(d2U - dU*dffI)
		gg1 = gg-gg2
		gg1p = ggp-gg2p
		RHS(2) = -gg1p/gg1*f(2) + aa*aa*gg/gg1*f(1)
	end if

!!	print *, 'RHS=',RHS
!!	STOP
!	if (abs(x)<1) print *, 'RHS=',RHS
	do i1=1,NEQ/2
		DYDX(i1)=real(RHS(i1))
		DYDX(i1+NEQ/2)=aimag(RHS(i1))
	end do
end subroutine Derivs

!---------------------------------------------------------------------------------

	SUBROUTINE make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
	use parameters
	implicit none
	integer :: NI,NO,i1,read_grid,N
	real*8 :: ZI(NI),ZO(NO),r0,rm,rf

	if (read_grid==1) then
	N=NI+NO-1
		open(unit=23,file='z.out')
		do i1=1,NI
			read(23,*) ZI(i1)
		end do
		do i1=2,NO
			read(23,*) ZO(NO+1-i1)
		end do
		ZO(NO) = zI(NI)
	close(23)
	else

	do i1=1,NI
		ZI(i1)=r0+real(i1-1)*(rm-r0)/real(NI-1)
!			T(i1)=pi*real(i1-1)/real(N-1)
	end do
	do i1=1,NO
		ZO(i1)=rf+real(i1-1)*(rm-rf)/real(NO-1)
!			T(i1)=pi*real(i1-1)/real(N-1)
	end do
!	Z=cos(T)
	end if

	

	end subroutine make_grid
!---------------------------------------------------------------------------------

subroutine jacobian()
end subroutine jacobian
!---------------------------------------------------------------------------------
subroutine least_squares(m,n,x,fvec,iflag) !function called by lmdif for nonlinear least squares problem
	use variables
	implicit none
	integer :: m,n,iflag
	double precision :: x(n),fvec(m)
	real*8 :: k1,k2,k3,k0,a1
	complex*16 :: a2,w0,w1,w2,w3,w4,k1p,k2p,k1m,k2m,func(3)
	w1=wwf(1)
	w2=wwf(2)
	w3=wwf(3)
!	w4=wwf(2,2)
	k1=aaf(1)
	k2=aaf(2)
	k3=aaf(3)
	
	a1 = x(1)
	a2 = cmplx(x(2),x(5))
!	a2=cmplx(x(2),x(6))
	w0=cmplx(x(3),x(6))
	k0=x(4)

if (1==2) then
	print *, 'lmdif'
	print *, 'w1,w2=',w1,w2
	print *, 'k1p,k1m,k2p,k2m=',k1p,k1m,k2p,k2m
	print *, 'w0,k0=',w0,k0
	print *, 'a1,a2=',a1,a2
end if

	func = wwf - w0 - a1*(kkf-k0) - a2*(kkf-k0)**2

!	func(1) = w1 - w0 - a1*(k1-k0)**2
!	func(2) = w2 - w0 - a1*(k2-k0)**2
!	func(3) = w3 - w0

!	func(1) = k1p - k0 + a1*sqrt(w1-w0) - a2*(w1-w0)
!	func(2) = k2p - k0 + a1*sqrt(w2-w0) - a2*(w2-w0)
!	func(3) = k1m - k0 - a1*sqrt(w3-w0) - a2*(w3-w0)
!	func(4) = k2m - k0 - a1*sqrt(w4-w0) - a2*(w4-w0)

!	print *, 'func=',func
	fvec(1:3)=real(func)
	fvec(4:6)=aimag(func)
end subroutine least_squares
!---------------------------------------------------------------------------------
subroutine evaluate_k(a1,a2,w0,k0,w1,w2,kkf)
	implicit none
	complex*16 :: a1,a2,w0,k0,w1,w2,kkf(2,2)
	kkf(1,1) =  k0 - a1*sqrt(w1-w0) + a2*(w1-w0)
	kkf(1,2) =  k0 + a1*sqrt(w2-w0) + a2*(w2-w0)
	kkf(2,1) =  k0 - a1*sqrt(w2-w0) + a2*(w2-w0)
	kkf(2,2) =  k0 + a1*sqrt(w1-w0) + a2*(w1-w0)
end subroutine evaluate_k
!---------------------------------------------------------------------------------
subroutine evaluate_w(a1,a2,k0,w0,kkf,wwf)
	implicit none
	real*8 :: k0,kkf(3)
	complex*16 :: a1,a2,w0,w1,w2,wwf(3)
	wwf = w0 + a1*(kkf-k0) + a2*(kkf-k0)**2
end subroutine evaluate_w
!---------------------------------------------------------------------------------

