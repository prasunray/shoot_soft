! planar viscoelastic shear layers, Giesekus model
!  shoot.F90
!10/24/11: modified version of shoot2tw.F90 -- now spatial
! 2/26/09: modified version of shoot2.F90 -- Cartesian
! 4/27/06: modified version of shoot.F90 -- matches solutions at rm  
!
! 

!---------------------------------------------------------------------------------
module variables
implicit none
save
integer :: Nwr,Nwi, flag_display
real*8 :: r0,rf,rm,r0i,rfi,rmi
complex*16 :: bb !frequency, x-wavenumber,y-wavenumber,total wavenumber squared
real*8 :: Re,St,wr1,wi1,wr2,wi2,ar1,ai1 !Rayleigh, Prandtl number
complex*16, allocatable, dimension(:,:) :: wwf,aaf
complex*16 :: ww,aa,kk,kk2,ll
complex*16, allocatable, dimension(:) :: uu,vv,ss,tt,pp,ppy,vvy,vvyy !velocity components, temperature, pressure
real*8 :: ue,theta,U,dU,d2U,d3U
complex*16, allocatable, dimension(:,:,:) :: P_orthoI,P_orthoO
integer, allocatable, dimension(:) :: j_orthoI,j_orthoO
integer :: flag_orthoI,flag_orthoO,flag_symmetry,mean_type
integer :: i_rf
real*8 :: lg,sg, Sc !Input parameters, lambda, Greek letter I don't know, Schmidt number.
real*8, parameter :: rescale_tol = 1.0e-10
real, allocatable, dimension(:) :: ZF
complex*16, allocatable, dimension(:,:) :: fvecAc
end module variables
!---------------------------------------------------------------------------------
module polymer_variables
implicit none
save
integer :: model_type
complex*16 :: phi,psi,tau,A11,A12,A22,phip,psip,taup,A11p,A12p,A22p
complex*16 :: G221,G222,G223,G221p,G222p,G223p
complex*16 :: c131,c132,c133,c131p,c132p,c133p
complex*16 :: c31,c32,c31p,c32p
complex*16 :: chi,chip,F22,F22p
real*8 :: a11m,a12m,a22m,Da11m,Da12m,Da22m,D2a11m,D2a12m,D2a22m !mean polymeric stress and derivatives
complex*16 :: L1,L1p
complex*16 :: M1,M2,M3,M1p,M2p,M3p
complex*16 :: N1,N2,N3,N1p,N2p,N3p
complex*16 :: R1,R2,R3,R4
complex*16 :: S1,S2,S3,S4
complex*16 :: P1,P2,P3,B1,B2,P1p,P2p,P3p,B1p,B2p
complex*16 :: c231,c232,c233,c231p,c232p,c233p
complex*16 :: c1231,c1232,c1233,c1231p,c1232p,c1233p
real*8 :: CM11,CM12,CM22,DCM11,DCM12,DCM22,D2CM11,D2CM12,D2CM22,CMKK !FENE-P: mean conformation tensor & derivatives
real*8 :: L_FENE !FENE-P: maximum extensibility

end module polymer_variables
!---------------------------------------------------------------------------------

!---------------------------------------------------------------------------------
	  
	  program shoot
	  use grid
	  use parameters
	  use variables
	  use hybrd_variables
	  use dlsode_variables
	  use spline_variables
	  use polymer_variables
	  
	  implicit none
	  integer :: i1,j1
	  integer, parameter :: scaling_factor=0
		external OS_integrate
 

		open(unit=9,file='input2pw.in')
		read(9,*) model_type
		read(9,*) lg
		read(9,*) sg
		read(9,*) Sc
		read(9,*) ar1
		read(9,*) ai1
		read(9,*) Nwr
		read(9,*) Nwi
		read(9,*) wr1
		read(9,*) wi1
		read(9,*) wr2
		read(9,*) wi2
	    read(9,*) Re
		read(9,*) theta
		read(9,*) Ue
		read(9,*) NI
		read(9,*) NO
		read(9,*) r0i
		read(9,*) rmi
		read(9,*) rfi
		read(9,*) read_grid
		read(9,*) flag_symmetry
		read(9,*) mean_type
		read(9,*) flag_display
	if (1==1) then
		read(9,*) Nspline
	end if
	if (model_type==3) then
		read(9,*) L_FENE
		print *, 'L_FENE=',L_FENE
	end if
	
	
	
		close(9)
		
		rf = rfi
		rm = rmi		
		r0 = r0i 

		allocate(wwf(Nwr,Nwi),aaf(Nwr,Nwi),infoA(Nwr,Nwi),fvecAc(Nwr,Nwi))
		do i1=1,Nwr
			do j1=1,Nwi
				wwf(i1,j1)=cmplx((wr1+real(i1-1)*(wr2-wr1)/real(Nwr-1+1.0e-16)),(wi1+real(j1-1)*(wi2-wi1)/real(Nwi-1+1.0e-16)))
			end do
		end do

	    if (mean_type==1) then
			if (scaling_factor==1) then
			Re=Re/1.7208
			ww=ww/1.7208
			aa_guess=aa_guess/1.7208
			end if
				
		end if
		print *, 'NI,NO=',NI,NO

		print *, 'Re=',Re
		print *, 'lg,Sc=',lg,Sc
		print *, 'Ue,theta=',Ue,theta
!		ww=2.0*pi*St
		print *, 'w=',ww
!		bb=0.
	

		tol=1.e-8
		print *, 'set parameters'
		
		!ALLOCATE VARIABLES
		allocate(ZI(NI),ZO(NO),ZF(NI+NO-1))
	  
	  
	  
	  !make grid
	 
	 call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
		open(unit=9,file='rI.out')
 			do i1=1,NI
		!write(9,*) Z(N+1-i1)
			write(9,*) ZI(i1)
			end do
		close(9)
		
		open(unit=9,file='rO.out')
 			do i1=1,NO
		!write(9,*) Z(N+1-i1)
			write(9,*) ZO(i1)
			end do
		close(9)
	   print *, 'generated grid'
	 
	 
		if (mean_type==1) then
	!intialize spline for Blasius mean flow
			allocate(zspline(Nspline),yspu(Nspline,2),uspline(Nspline,2))
			do i1=1,Nspline
				zspline(i1) = r0 + real(i1-1)/real(Nspline-1)*(rf-r0)
				call calculate_velocity(zspline(i1),uspline(i1,1),dU,uspline(i1,2))
			end do
	call spline(zspline,uspline(:,1),Nspline,0.0,1.0e31,yspu(:,1))
	call spline(zspline,uspline(:,2),Nspline,0.0,1.0e31,yspu(:,2))
	end if



	!output mean flow
	open(unit=9,file='mean.out')
	open(unit=10,file='polymer_mean.out')
	do i1=1,NI
			call mean_flow(mean_type,ZI(i1),theta,Ue,rf,U,dU,d2U,d3U)
			if (mean_type<101) call polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)
			
		write(9,'(5E28.16)') zI(i1),U,dU,d2U,d3U
		write(10,'(10E28.16)') zI(i1),a11m,da11m,d2a11m,a12m,da12m,d2a12m,a22m,da22m,d2a22m
	end do
	do i1=NO-1,1,-1
		call mean_flow(mean_type,ZO(i1),theta,Ue,rf,U,dU,d2U,d3U)
		if (mean_type<101) call polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)

		write(9,'(5E28.16)') zO(i1),U,dU,d2U,d3U
		write(10,'(10E28.16)') zO(i1),a11m,da11m,d2a11m,a12m,da12m,d2a12m,a22m,da22m,d2a22m

	end do
	close(9)
	close(10)
!	STOP
!	call test_interpolation()
	
	if (mean_type==5) then !rescale rI, rf
					call mean_flow(mean_type,rf,theta,Ue,rf,U,dU,d2U,d3U)
						if (abs(dU) <	rescale_tol) then
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NO-1)
								i1=i1+1
								call mean_flow(mean_type,zO(i1),theta,Ue,rf,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) rf = zO(i1)
							!	print *, 'i1,NO,z,dU',i1,NO,zO(i1),rf,dU								
								end do
						else
							rf=rfi
							call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NO-1)
								i1=i1+1
								call mean_flow(mean_type,zO(i1),theta,Ue,rf,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) rf = zO(i1)
							end do
						end if
					call mean_flow(mean_type,r0,theta,Ue,rf,U,dU,d2U,d3U)
						if (abs(dU) <	rescale_tol) then
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NI-1)
								i1=i1+1
								call mean_flow(mean_type,zI(i1),theta,Ue,rf,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) r0 = zI(i1)
							!	print *, 'z,dU',zI(i1),dU							
							end do
						else
							r0=r0i
							call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NI-1)
								i1=i1+1
								call mean_flow(mean_type,zI(i1),theta,Ue,rf,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) r0 = zI(i1)
							end do
						end if							
				rm  = r0 + (rf-r0)*real(NI)/real(NI+NO)								
				call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)					
					print *, '***rescaled***, r0,rf=', r0,rm,rf,rescale_tol				
				end if


	!call minimizing routine with (fcn=OS_integrate)
		allocate(P_orthoI(Neq/4,Neq/4,NI),j_orthoI(NI))
		P_orthoI=0.0
		j_orthoI=0
		flag_orthoI=1

		allocate(P_orthoO(Neq/4,Neq/4,NO),j_orthoO(NO))
		P_orthoO=0.0
		j_orthoO=0
		flag_orthoO=1
		print *, 'r0,rm,rf=',r0,rm,rf
aa_guess(1)=ar1
aa_guess(2)=ai1
do i1=1,Nwr
	do j1=1,Nwi
	ww=wwf(i1,j1)
		print *, i1,j1,'ww=',ww
		P_orthoI=0.0
			j_orthoI=0
			flag_orthoI=1
		P_orthoO=0.0
			j_orthoO=0
			flag_orthoO=1
		
		call OS_integrate(2,aa_guess,fvec,info)
		
	
	print *, 'total orthogonalizations:',sum(j_orthoI),sum(j_orthoO)
!	aa=cmplx(aa_guess(1),aa_guess(2))
	infoA(i1,j1)=info
	
	fvecAc(i1,j1)=cmplx(fvec(1),fvec(2))
  print *, 'fvec=',fvecAc(i1,j1)
	end do
	end do
	!calculate eigenvectors
	!*MODIFY AS NEEDED WITH ORTHOGONALIZATION FACTORS MATRICES
!!	call calculate_eigenvectors()
    !output
!!	call output()
  	open(unit=22,file='fvec.out')
	do i1=1,Nwr
		do j1=1,Nwi
			write(22,'(2E28.16)') real(fvecAc(i1,j1)),aimag(fvecAc(i1,j1))
		end do
	end do
	close(22)

	open(unit=23,file='wr.out')
	do i1=1,Nwr
		write(23,*) real(wwf(i1,1))
	end do
	close(23)

	open(unit=24,file='wi.out')
	do j1=1,Nwi
		write(24,*) aimag(wwf(1,j1))
	end do
	close(24)	
	
	
	      end program shoot

!---------------------------------------------------------------------------------
	   
	 subroutine OS_integrate(nx,x,fvec,iflag)
		use variables
		use parameters
		use dlsode_variables
		use grid
		implicit none
	        integer :: nx,iflag,i1,j1,k1,k10
		double precision :: x(nx),fvec(nx),za,zb
		real*8 :: YSTART(NEQ,NEQ/4),angle(NEQ/4*(NEQ/4-1)/2)
		real*8, parameter :: h1=0.0001,hmin=1.e-14,eps=1.e-12
		complex*16 :: F1(NEQ/2,NEQ/4),F2(NEQ/2,NEQ/4),det_matrix(Neq/2,Neq/2), det,determinant,gamma
		real*8, parameter :: angle_threshold=5.0
		external Derivs,RKQS,jacobian
		
!		print *, 'enter OS_integrate'
	
		aa=cmplx(x(1),x(2))
		kk2 = aa*aa

	
		!integrate OS_equation twice with different initial conditions
		!integrate with subroutine Derivs 
	
		!*GENERATE INITIAL CONDITIONS
		YSTART=0.0
		call mean_flow(mean_type,r0,theta,Ue,rf,U,dU,d2U,d3U)
		gamma=ii*(aa*U-ww)
	    bb=sqrt(aa**2+Re*gamma)
		call inner_conditions(NEQ,r0,aa,bb,gamma,Re,U,lg,Sc,YSTART,flag_symmetry)
	if (((mean_type==5) .OR. (mean_type==2)) .and. (r0>0.0))   &
		call inner_conditions(NEQ,r0,aa,bb,gamma,Re,U,lg,Sc,YSTART,flag_symmetry+200)
        F1 =  cmplx(YSTART(1:NEQ/2,:),YSTART(NEQ/2+1:NEQ,:))		
		do k1=1,NI-1
			do j1=1,NEQ/4
				ISTATE=1
				za=ZI(k1)
				zb=ZI(k1+1)
!!			print *, 'j1, YSTART=', j1, YSTART(:,j1)
!!			print *, 'za,zb=', ZI(k1),ZI(k1+1)
				rwork=0.0
				iwork=0
				iwork(6)=50000
				call DLSODE(Derivs,NEQ,YSTART(:,j1),za,zb,1,rtol,atol,1,istate,0,rwork,lrw,iwork,liw,jacobian,mf)
				do i1=1,NEQ/2
					F1(i1,j1)=cmplx(YSTART(i1,j1),YSTART(i1+NEQ/2,j1))
				end do
    
!!			print *, 'ISTATE=',ISTATE
!!			print *, 'YSTART=',YSTART(:,j1)
		end do
		
		if (flag_orthoI==1) then
		!*CHECK ANGLE AND ORTHOGONALIZE AS NEEDED
		call calculate_angle(NEQ,F1,angle)
		if (flag_display==10) print *, 'r,angle=',zI(k1),minval(abs(angle)),maxval(abs(F1))
	!	print *, 'angle=',minval(abs(angle))
		if ((minval(abs(angle))<angle_threshold) .OR. (k1==NI-1) .OR. (maxval(abs(F1))>1.0e4)) then
			if (flag_display==1) 	print *, '***ORTHOGONALIZE***,r=',ZI(k1),minval(abs(angle)),maxval(abs(F1))
			j_orthoI(k1)=1
			call orthogonalize(NEQ,P_orthoI(:,:,k1),F1)
			
			if (k1==NI-1) then
!				flag_orthoI=2
			end if
		end if
		elseif ((flag_orthoI==2) .AND. (j_orthoI(k1)==1)) then
!!			print *, '***ORTHOGONALIZE***, r=',ZI(k1)
			F1=matmul(F1,P_orthoI(:,:,k1))
		end if
											
		!*SAVE ORTHOGONALIZATION FACTORS/MATRICES
		
		
			YSTART(1:NEQ/2,:)=real(F1)
		    YSTART(NEQ/2+1:NEQ,:)=aimag(F1)
	end do		
		
		!*GENERATE OUTER CONDITIONS		
		call mean_flow(mean_type,rf,theta,Ue,rf,U,dU,d2U,d3U)
		gamma=ii*(aa*U-ww)
		bb = -gamma*Re - 2.0*aa*aa
		kk=aa**4 + aa*aa*gamma*Re
		call outer_conditions(NEQ,mean_type,rf,aa,bb,gamma,kk,Re,U,lg,Sc,F2)
!		print *, 'outer conditions:',F2

                 k10=1
                 if (mean_type==500) then
                        do k1=1,NO-1
                                call mean_flow(mean_type,zO(k1),theta,Ue,rf,U,dU,d2U,d3U)
                                if (abs(dU) < 1.0e-10) then
                                        k10 = k1
                                end if
                        end do

                        call mean_flow(mean_type,zO(k10),theta,Ue,rf,U,dU,d2U,d3U)
                        gamma=ii*(aa*U-ww)
                        bb = -gamma*Re - 2.0*aa*aa
                        kk=aa**4 + aa*aa*gamma*Re
                        call outer_conditions(NEQ,mean_type,zO(k10),aa,bb,gamma,kk,Re,U,lg,Sc,F2)
                end if

	
		YSTART(1:NEQ/2,:)=real(F2)
		YSTART(NEQ/2+1:NEQ,:)=aimag(F2)
		do k1=k10,NO-1
			do j1=1,NEQ/4
				ISTATE=1
				za=ZO(k1)
				zb=ZO(k1+1)
		!	print *, 'j1, YSTART=', j1, YSTART(:,j1)
		!	print *, 'za,zb=', Z(k1),Z(k1+1)
				rwork=0.0
				iwork=0
				iwork(6)=50000
			
				call DLSODE(Derivs,NEQ,YSTART(:,j1),za,zb,1,rtol,atol,1,istate,0,rwork,lrw,iwork,liw,jacobian,mf)

				do i1=1,NEQ/2
					F2(i1,j1)=cmplx(YSTART(i1,j1),YSTART(i1+NEQ/2,j1))
				end do
    
		!	print *, 'ISTATE=',ISTATE
		!	print *, 'YSTART=',YSTART(:,j1)
	
		end do
		
		if (flag_orthoO==1) then
		!*CHECK ANGLE AND ORTHOGONALIZE AS NEEDED
		call calculate_angle(NEQ,F2,angle)
		
if (flag_display==10) 		print *, 'r,angle=',zO(k1),minval(abs(angle)),maxval(abs(F2))
		if ((minval(abs(angle))<angle_threshold) .OR. (k1==NO-1) .OR. (maxval(abs(F2))>1.0e4)) then
	if (flag_display==1) 		print *, '***ORTHOGONALIZE0***,r=',ZO(k1), minval(abs(angle)),maxval(abs(F2(:,2)))
			j_orthoO(k1)=1
			call orthogonalize(NEQ,P_orthoO(:,:,k1),F2)
			
			if (k1==NO-1) then
!				flag_orthoO=2
			end if
		end if
		elseif ((flag_orthoO==2) .AND. (j_orthoO(k1)==1)) then
!!			print *, '***ORTHOGONALIZE***, r=',ZO(k1)
			F2=matmul(F2,P_orthoO(:,:,k1))
		end if
											
		!*SAVE ORTHOGONALIZATION FACTORS/MATRICES
		
		
			YSTART(1:NEQ/2,:)=real(F2)
		    YSTART(NEQ/2+1:NEQ,:)=aimag(F2)
	end do		

	

		!Compute determinant using results from integration
		!*MODIFY DETERMINANT MATRIX APPROPRIATELY
		det_matrix(:,1:2)=F1
		det_matrix(:,3:4)=-F2
		
	!	det_matrix(1,:)=(/1.0, 2.0, 3.0, 4.0/)
!		det_matrix(2,:)=0.5*det_matrix(1,:)
!!		det_matrix(4,:)=(/1.0, 0.0, 2.0, 0.0/)	
		det=determinant(4,det_matrix)
!		print *, 'det_matrix='
if (1==2) then
		open(unit=22,file='detr.out')
		open(unit=23,file='deti.out')
		do i1=1,NEQ/2
		write(22,('(1000E28.16)')), real(det_matrix(i1,:))
				write(23,('(1000E28.16)')), aimag(det_matrix(i1,:))

		print *, (det_matrix(i1,:))
		end do
		close(22)
		close(23)
		print *, 'det=',det
end if		
		fvec(1)=real(det)
		fvec(2)=aimag(det)
	
!		fvec(1) = x(1)**2-pi
!		fvec(2)=0.0
!!!	print *, 'aa,fvec=',aa,fvec
!!!	print *, 'ue,lg,sc,Re=',ue,lg,sc,Re
!!!	print *, 'det_matrix=',det_matrix

!!			print *, 'exit OS_integrate'
	!	STOP	
	end subroutine OS_integrate
!---------------------------------------------------------------------------------

!---------------------------------------------------------------------------------
subroutine Derivs(NEQ,x,Y,DYDX)
	use variables
	use polymer_variables
	use parameters
	implicit none
	integer :: NEQ,i1,j1
	double precision :: x, Y(*),DYDX(*)
	complex*16 :: MM(NEQ/2,NEQ/2),NNI(NEQ/2,NEQ/2),RHS(NEQ/2),f(NEQ/2),gamma
	complex*16 :: R1g,R2g,R3g,R4g,S1g,S2g,S3g,S4g,invphi,RHSg(NEQ/2)
	real*8 :: MD(8)
!	print *, 'enter derivs'
	
	do i1=1,NEQ/2
		f(i1)=cmplx(Y(i1),Y(i1+NEQ/2))
	end do
	call mean_flow(mean_type,x,theta,Ue,rf,U,dU,d2U,d3U)
	if (mean_type<101) call polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)

	gamma=ii*(aa*U-ww)
!	call giesekus_model(gamma)
!!!	if (abs(x)<1) then 
!		print *, 'R=',R1,R2,R3,R4
!		print *, 'S=',S1,S2,S3,S4
	if (model_type==1) then
		call oldroyd_model(gamma)
	elseif (model_type==2) then
		call giesekus_model(gamma)
	elseif (model_type==3) then
		call FENE_model2(gamma)
	end if
if (1==2) then
	print *, 'oldroyd:', R1,R2,R3,R4
	print *, S1,S2,S3,S4
	call giesekus_model(gamma)
	print *, 'giesekus:', R1,R2,R3,R4
	print *, S1,S2,S3,S4
	STOP
end if	
MM=0.0
	MM(1,2) = 1.0
	MM(2,1) = gamma*Re+kk2*Sc-R3*(1.0-Sc)
	MM(2,2) = -(1.0-Sc)*R2
	MM(2,3) = Re*dU-(1.0-Sc)*R4
	MM(2,4) = ii*aa*Re
	MM(3,1) = -ii*aa
	MM(4,1) = (1.0-Sc)*S3 
	MM(4,2) = (1.0-Sc)*S2 - ii*aa*Sc

	MM(4,3) = -gamma*Re-kk2*Sc+(1.0-Sc)*S4
!!	print *, 'kk2=',kk2
!!	print *, 'MM=',MM

!!	MM=0.0
	NNI=0.0
!!	MM(1,2) = 1.0
!!	MM(2,1) = gamma*Re+kk2
	
!!	MM(2,3) = Re*(dU)
!!	MM(2,4) = ii*aa*Re
!!	MM(3,1) = -ii*aa
!!	MM(4,2) = -ii*aa/Re

!!	MM(4,3) = -gamma-kk2/Re

	NNI(1,1) = 1.0
	NNI(2,2) = 1.0/((1.0-Sc)*R1+Sc)
	NNI(3,3) = 1.0
	NNI(4,2) = ((1.0-Sc)*S1)/(Re*((1.0-Sc)*R1)+Sc)
	NNI(4,4) = 1.0/Re
	
	MM = matmul(NNI,MM)
	RHS=matmul(MM,f)



!!	print *, 'RHS=',RHS
!!	STOP
!	if (abs(x)<1) print *, 'RHS=',RHS
	do i1=1,NEQ/2
		DYDX(i1)=real(RHS(i1))
		DYDX(i1+NEQ/2)=aimag(RHS(i1))
	end do
end subroutine Derivs
!---------------------------------------------------------------------------------

!---------------------------------------------------------------------------------

	SUBROUTINE make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
	use parameters
	implicit none
	integer :: NI,NO,i1,read_grid,N
	real*8 :: ZI(NI),ZO(NO),r0,rm,rf

	if (read_grid==1) then
	N=NI+NO-1
		open(unit=23,file='z.out')
		do i1=1,NI
			read(23,*) ZI(i1)
		end do
		do i1=2,NO
			read(23,*) ZO(NO+1-i1)
		end do
		ZO(NO) = zI(NI)
	close(23)
	else

	do i1=1,NI
		ZI(i1)=r0+real(i1-1)*(rm-r0)/real(NI-1)
!			T(i1)=pi*real(i1-1)/real(N-1)
	end do
	do i1=1,NO
		ZO(i1)=rf+real(i1-1)*(rm-rf)/real(NO-1)
!			T(i1)=pi*real(i1-1)/real(N-1)
	end do
!	Z=cos(T)
	end if

	

	end subroutine make_grid
!---------------------------------------------------------------------------------	
subroutine jacobian()
end subroutine jacobian
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
function dot_productc(A,B)
	implicit none
		complex*16, dimension(3) :: A,B
		complex*16 :: dot_productc
		
		dot_productc = sum(A*B)
end function dot_productc

