! Viscoelastic spatiotemporal linear stability solver: find saddle point with S specified

! 7/17/12: modified version of shoot2absf.F90, Oldroyd-B model
! 2/20/12: modified version of shoot2abs.F90, search for S such that w_i=0, in the code ue=S.
! 7/25/11: modified version of shoot2w.F90, search for absolute instabilities following
!		   Monkewitz (1988).
! 2/26/09: modified version of shoot2.F90 -- Cartesian
! 4/27/06: modified version of shoot.F90 -- matches solutions at rm
!
!
!---------------------------------------------------------------------------------
module variables
implicit none
save
integer :: Nwr,Nwi,Nvary,a1f
real*8 :: r0,rf,rm,rel_tol,err_w0,scratch,alpha_relax,r0i,rfi,rmi
real*8 :: Re,St,wr1,wr2,wi1,wi2 ,delta
complex*16 :: ww,aa,bb,kk,kk2,wwf(2,2),kkf(2,2),kmm(2,2),wwg(2,2),kkg(2,2),ww0g,kk0g,kktemp(2,2)
complex*16, allocatable, dimension(:,:) :: aaf
complex*16, allocatable, dimension(:) :: ww0,kk0,cc1,cc2,wwff,aaff,cc1ff,cc2ff,uu,vv,tt,pp,vvy,vvyy,cc1fff,cc2fff
real*8 :: ue,theta,U,dU,d2U,d3U
integer :: vary_Re,vary_ue,vary_theta,vary_lg,vary_Sc
real*8 :: uei,thetai,Rei,uef,thetaf,Ref,lgi,lgf,Sci,Scf
complex*16, allocatable, dimension(:,:,:) :: P_orthoI,P_orthoO
integer, allocatable, dimension(:) :: j_orthoI,j_orthoO
integer :: flag_orthoI,flag_orthoO,flag_symmetry,mean_type
real*8 :: lg,sg, Sc !Input parameters, lambda, Greek letter I don't know, Schmidt number.
real*8 :: ue1,ue2,err_ue,ue_tol,ueg
real*8, allocatable, dimension(:) :: uea
complex*16, allocatable, dimension(:) :: wwfff,aafff
complex*16 :: dwdue,dkdue
integer, parameter :: flag_display=0
real*8, parameter :: rescale_tol = 1.0e-8
end module variables
!---------------------------------------------------------------------------------
module polymer_variables
implicit none
save
integer :: model_type
complex*16 :: phi,psi,tau,A11,A12,A22,phip,psip,taup,A11p,A12p,A22p
complex*16 :: G221,G222,G223,G221p,G222p,G223p
complex*16 :: c131,c132,c133,c131p,c132p,c133p
complex*16 :: c31,c32,c31p,c32p
complex*16 :: chi,chip,F22,F22p
real*8 :: a11m,a12m,a22m,Da11m,Da12m,Da22m,D2a11m,D2a12m,D2a22m !mean polymeric stress and derivatives
complex*16 :: L1p
complex*16 :: M1,M2,M3,M1p,M2p,M3p
complex*16 :: N1,N2,N3,N1p,N2p,N3p
complex*16 :: R1,R2,R3,R4
complex*16 :: S1,S2,S3,S4
complex*16 :: P1,P2,P3,B1,B2,P1p,P2p,P3p,B1p,B2p
complex*16 :: c231,c232,c233,c231p,c232p,c233p
complex*16 :: c1231,c1232,c1233,c1231p,c1232p,c1233p
real*8 :: CM11,CM12,CM22,DCM11,DCM12,DCM22,D2CM11,D2CM12,D2CM22,CMKK !FENE-P: mean conformation tensor & derivatives
real*8 :: L_FENE,L_FENEi,L_FENEf !FENE-P: maximum extensibility
integer :: vary_L_FENE
end module polymer_variables

!---------------------------------------------------------------------------------

!---------------------------------------------------------------------------------

	  program shoot
	  use grid
	  use parameters
	  use variables
	  use hybrd_variables
	  use dlsode_variables
	  use spline_variables
	  use lmdif_variables
	  use polymer_variables


	  implicit none
	  integer :: a1,i1,j1,k1,l1,k1g,count
	  integer, parameter :: k1max=1000
		external OS_integrate,least_squares


		open(unit=9,file='input2pabs.in')
		read(9,*) model_type
		read(9,*) lgi
		read(9,*) sg
		read(9,*) Sci

		read(9,*) kkf(1,1) !initial eigenvalue guesses (complex)
		read(9,*) kkf(2,1)
		read(9,*) kkf(1,2)
		read(9,*) kkf(2,2)

		read(9,*) wwf(1,1) ! frequencies (complex)
		read(9,*) wwf(2,1)
		read(9,*) wwf(1,2)
		read(9,*) wwf(2,2)

		read(9,*) Rei
		read(9,*) thetai
		read(9,*) Uei
		read(9,*) NI
		read(9,*) NO
		read(9,*) r0i
		read(9,*) rmi
		read(9,*) rfi
		read(9,*) read_grid
		read(9,*) flag_symmetry
		read(9,*) mean_type
!	if (mean_type==1) then
		read(9,*) Nspline
!	end if
		read(9,*) scratch
		read(9,*) rel_tol
		read(9,*) vary_Re,Ref
		read(9,*) vary_theta,thetaf
		read(9,*) vary_ue,uef
		read(9,*) vary_lg,lgf
		read(9,*) vary_Sc,Scf
		read(9,*) Nvary
		read(9,*) alpha_relax
		if (model_type==3) then
		read(9,*) L_FENEi
		read(9,*) vary_L_FENE, L_FENEf
		end if

		close(9)
		allocate(aaf(2,2),infoA(2,2),fvecA(2,2))
!		allocate(wwf(Nwr,Nwi),aaf(Nwr,Nwi),infoA(Nwr,Nwi),fvecA(Nwr,Nwi))
!		do i1=1,Nwr
!			do j1=1,Nwi
!				wwf(i1,j1)=cmplx((wr1+real(i1-1)*(wr2-wr1)/real(Nwr-1+1.0e-16)),(wi1+real(j1-1)*(wi2-wi1)/real(Nwi-1+1.0e-16)))
!			end do
!		end do

		rf = rfi
		rm = rmi
		r0 = r0i

		if (Nvary < 1) Nvary=1
		print *, 'Ref,thetaf,uef,Nvary:',Ref,thetaf,uef,Nvary
		allocate(wwff(1000),aaff(1000),cc1ff(1000),cc2ff(1000))
		allocate(wwfff(Nvary),aafff(Nvary),uea(Nvary),cc1fff(Nvary),cc2fff(Nvary))

	    if (mean_type==1) then
			Re=Re/1.7208
			wwf=wwf/1.7208
			aa_guess=aa_guess/1.7208
		end if

		print *, 'Re=',Rei
				bb=0.0

		tol=1.e-8
		err_w0=10.0
		print *, 'set parameters'

		!ALLOCATE VARIABLES
		allocate(ZI(NI),ZO(NO))

	  !make grid

	  call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
		open(unit=9,file='rI.out')
 			do i1=1,NI
		!write(9,*) Z(N+1-i1)
			write(9,*) ZI(i1)
			end do
		close(9)

		open(unit=9,file='rO.out')
 			do i1=1,NO
		!write(9,*) Z(N+1-i1)
			write(9,*) ZO(i1)
			end do
		close(9)
	   print *, 'generated grid'


		if (mean_type==1) then
	!intialize spline for Blasius mean flow
			allocate(zspline(Nspline),yspu(Nspline,2),uspline(Nspline,2))
			do i1=1,Nspline
				zspline(i1) = r0 + real(i1-1)/real(Nspline-1)*(rf-r0)
				call calculate_velocity(zspline(i1),uspline(i1,1),dU,uspline(i1,2))
			end do
	call spline(zspline,uspline(:,1),Nspline,0.0,1.0e31,yspu(:,1))
	call spline(zspline,uspline(:,2),Nspline,0.0,1.0e31,yspu(:,2))
	end if

	L_FENE=L_FENEi
	!output mean flow
	open(unit=9,file='mean.out')
	open(unit=10,file='polymer_mean.out')
	do i1=1,NI
			call mean_flow(mean_type,ZI(i1),thetai,uei,U,dU,d2U,d3U)
			call polymer_mean(lgi,sg,Sci,U,dU,d2U,d3U)

		write(9,'(5E28.16)') zI(i1),U,dU,d2U,d3U
		write(10,'(10E28.16)') zI(i1),a11m,da11m,d2a11m,a12m,da12m,d2a12m,a22m,da22m,d2a22m
	end do
	do i1=NO-1,1,-1
		call mean_flow(mean_type,ZO(i1),thetai,Uei,U,dU,d2U,d3U)
		call polymer_mean(lgi,sg,Sci,U,dU,d2U,d3U)


		write(9,'(5E28.16)') zO(i1),U,dU,d2U,d3U
		write(10,'(10E28.16)') zO(i1),a11m,da11m,d2a11m,a12m,da12m,d2a12m,a22m,da22m,d2a22m

	end do
	close(9)
	close(10)


	!call minimizing routine with (fcn=OS_integrate)
		allocate(P_orthoI(Neq/4,Neq/4,NI),j_orthoI(NI))
		P_orthoI=0.0
		j_orthoI=0
		flag_orthoI=1

		allocate(P_orthoO(Neq/4,Neq/4,NO),j_orthoO(NO))
		P_orthoO=0.0
		j_orthoO=0
		flag_orthoO=1
		allocate(kk0(k1max),ww0(k1max),cc1(k1max),cc2(k1max))
		Re=Rei
		theta=thetai
		ue=uei
		lg = lgi
		Sc = Sci
		k1g = 1
		L_FENE = L_FENEi


		if (mean_type==5) then !rescale rI, rf
					call mean_flow(mean_type,rf,theta,Ue,U,dU,d2U,d3U)
						if (abs(dU) <	rescale_tol) then
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NO-1)
								i1=i1+1
								call mean_flow(mean_type,zO(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) rf = zO(i1)
							!	print *, 'i1,NO,z,dU',i1,NO,zO(i1),rf,dU
								end do
						else
							rf=rfi
							call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NO-1)
								i1=i1+1
								call mean_flow(mean_type,zO(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) rf = zO(i1)
							end do
						end if
					call mean_flow(mean_type,r0,theta,Ue,U,dU,d2U,d3U)
						if (abs(dU) <	rescale_tol) then
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NI-1)
								i1=i1+1
								call mean_flow(mean_type,zI(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) r0 = zI(i1)
							!	print *, 'z,dU',zI(i1),dU
							end do
						else
							r0=r0i
							call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NI-1)
								i1=i1+1
								call mean_flow(mean_type,zI(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) r0 = zI(i1)
							end do
						end if
				rm  = r0 + (rf-r0)*real(NI)/real(NI+NO)
				call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
					print *, '***rescaled***, r0,rf=', r0,rm,rf,rescale_tol
				end if

		do l1=1,Nvary
			if (vary_Re==1) Re=Rei + real(l1-1)/real(Nvary-1)*(Ref-Rei)
			if (vary_ue==1) ue=uei + real(l1-1)/real(Nvary-1)*(uef-uei)
!!			ue=1./ue
			if (vary_theta==1) then
				theta=thetai + real(l1-1)/real(Nvary-1)*(thetaf-thetai)
				if (mean_type==5) then !rescale rI, rf
					call mean_flow(mean_type,rf,theta,Ue,U,dU,d2U,d3U)
						if (abs(dU) <	rescale_tol) then
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NO-1)
								i1=i1+1
								call mean_flow(mean_type,zO(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) rf = zO(i1)
							end do
						else
							rf=rfi
							call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NO-1)
								i1=i1+1
								call mean_flow(mean_type,zO(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) rf = zO(i1)
							end do
						end if
					call mean_flow(mean_type,r0,theta,Ue,U,dU,d2U,d3U)
						if (abs(dU) <	rescale_tol) then
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NI-1)
								i1=i1+1
								call mean_flow(mean_type,zI(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) r0 = zI(i1)
							!	print *, 'z,dU',zI(i1),dU
							end do
						else
							r0=r0i
							call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
							i1=1
							do while((abs(dU)<rescale_tol) .AND. i1<NI-1)
								i1=i1+1
								call mean_flow(mean_type,zI(i1),theta,Ue,U,dU,d2U,d3U)
								if (abs(dU) < rescale_tol) r0 = zI(i1)
							end do
						end if
					rm  = r0 + (rf-r0)*real(NI)/real(NI+NO)
					call make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
					print *, '***rescaled***, r0,rf=', r0,rm,rf
				end if
			end if

			if (vary_lg==1) lg=lgi + real(l1-1)/real(Nvary-1)*(lgf-lgi)
			if (vary_Sc==1) Sc=Sci + real(l1-1)/real(Nvary-1)*(Scf-Sci)
			if ((model_type==3) .AND. (vary_L_FENE==1)) L_FENE=L_FENEi + real(l1-1)/real(Nvary-1)*(L_FENEf-L_FENEi)

			print *, '+++ l1,Re,ue,theta,lg,Sc,k1g,wwg,dw0=',l1,Re,ue,theta,lg,Sc !,k1g,wwg,wwfff(l1-1)-wwfff(l1-2)
			if ((model_type==3) .AND. ((vary_L_FENE==1) .OR. (l1==1))) print *, 'L_FENE=',L_FENE
			if (l1==2) then
				wwf=wwg
				kkf=kkg


			elseif (l1>2) then !extrapolate guess
!!				wwf=wwg + wwfff(l1-1)-wwfff(l1-2)
!!				kkf=kkg + aafff(l1-1)-aafff(l1-2)
!!				call evaluate_k(cc1(k1g),cc2(k1g),ww0(k1g),kk0(k1g),wwf(1,1),wwf(2,1),kktemp)
!!				kkf=kktemp

				wwf=wwg + (wwfff(l1-1)-wwfff(l1-2))
				print *, 'wwf=',wwf(1,:),wwf(2,:)

				call evaluate_k(cc1(k1g),cc2(k1g),ww0(k1g),kk0(k1g),wwf(1,1),wwf(2,1),kktemp)
				kkf=kktemp

			end if


			call loop(l1,k1g)

	if (l1==1) then
		open(unit=230,file='eig.out')
		open(unit=231,file='wwg.out')
		open(unit=232,file='kkg.out')

	else
		open(unit=230,file='eig.out',position='append')
		open(unit=231,file='wwg.out',position='append')
		open(unit=232,file='kkg.out',position='append')

	end if
		write(230,'(4E28.16)') real(wwfff(l1)),aimag(wwfff(l1)),real(aafff(l1)),aimag(aafff(l1))
		write(231,'(8E28.16)') real(wwg(:,:)),aimag(wwg(:,:))
		write(232,'(8E28.16)') real(kkg(:,:)),aimag(kkg(:,:))


	close(230)
	close(231)
	close(232)



	end do !l1


	      end program shoot
 !---------------------------------------------------------------------------------
subroutine loop(l1,k1g)
use grid
	  use parameters
	  use variables
	  use hybrd_variables
	  use dlsode_variables
	  use spline_variables
	  use lmdif_variables
	  use polymer_variables
	implicit none
	  integer :: a1,i1,j1,k1,l1,k1g,count
	  integer, parameter :: k1max=1000
	external OS_integrate,least_squares
k1g=1

			k1=0
		do while (((err_w0>rel_tol) .OR. (k1<2)) .AND. (k1<k1max))
			k1=k1+1
			if (k1 ==2) then
				wwf=(alpha_relax*wwf+(1.0-alpha_relax)*ww0(k1-1))
!				kkf = (alpha_relax*kkf + (1.0-alpha_relax)*kk0(k1-1))
				call evaluate_k(cc1(k1-1),cc2(k1-1),ww0(k1-1),kk0(k1-1),wwf(1,1),wwf(2,1),kktemp)
				kkf=kktemp
			elseif (k1>2) then
				ww0g = 2.0*ww0(k1-1)-ww0(k1-2)
				kk0g = 2.0*kk0(k1-1)-kk0(k1-2)
				wwf=(alpha_relax*wwf+(1.0-alpha_relax)*ww0g)
				call evaluate_k(cc1(k1-1),cc2(k1-1),ww0(k1-1),kk0(k1-1),wwf(1,1),wwf(2,1),kktemp)
				!kkf = (alpha_relax*kkf + (1.0-alpha_relax)*kk0g)
				kkf=kktemp
			end if

			print *, '--- k1,err_w0=',k1,err_w0
			do i1=1,2 !frequencies
				do j1=1,2 !downstream and upstream branches
					ww=wwf(i1,j1)
					aa_guess(1)=real(kkf(i1,j1))
					aa_guess(2)=aimag(kkf(i1,j1))
					print *, 'i1,j1,ww=',i1,j1,ww
					P_orthoI=0.0
					j_orthoI=0
					flag_orthoI=1

					P_orthoO=0.0
					j_orthoO=0
					flag_orthoO=1

					if (1==2) then
						if ((i1==1) .AND. (j1>2)) then
							aa_guess(1)=real(2*aaf(i1,j1-1)-aaf(i1,j1-2))
							aa_guess(2)=aimag(2*aaf(i1,j1-1)-aaf(i1,j1-2))
						elseif (i1>1) then
							if (i1==2) then
								aa_guess(1) = real(aaf(i1-1,j1))
								aa_guess(2) = aimag(aaf(i1-1,j1))
							elseif (i1>2) then
								aa_guess(1)=real(2*aaf(i1-1,j1)-aaf(i1-2,j1))
								aa_guess(2)=aimag(2*aaf(i1-1,j1)-aaf(i1-2,j1))
							end if
						end if
					end if

					call hybrd1(OS_integrate,2,aa_guess,fvec,tol,info,wa,lwa)

					print *, 'computed eigenvalue, info=',info
					print *, 'fvec=',sqrt(fvec(1)**2+fvec(2)**2)
					print *, 'i1,St,aa=',i1,ww,cmplx(aa_guess(1),aa_guess(2))
					if ((info .ne. 1) .AND. (sqrt(fvec(1)**2+fvec(2)**2)>1.0e-8))then
						print *,'could not find eigenvalue'
						STOP
					end if
					infoA(i1,j1)=info
					fvecA(i1,j1)=sqrt(fvec(1)**2+fvec(2)**2)
					aaf(i1,j1)=cmplx(aa_guess(1),aa_guess(2))
				end do
			end do

		print *, 'wwf=',wwf(1,1),wwf(1,2),wwf(2,1),wwf(2,2)
		print *, 'aaf=',aaf(1,1),aaf(1,2),aaf(2,1),aaf(2,2)
!	if (abs(aaf(1,1)-aaf(1,2))<1e-8) STOP
!	if (abs(aaf(2,1)-aaf(2,2))<1e-8) STOP

	!solve nonlinear least squares problem to get estimate for (w0,k0)
		if ((k1>=1) .AND. (1>=1)) then
			if ((k1==1) .AND. (l1==1)) then
			lm_x(1)=-4.24
			lm_x(2)=-1.75
			lm_x(5)=0.615
			lm_x(6)=0.35
		end if

			lm_x(3)=real(0.25*sum(wwf))
			lm_x(4)=real(0.25*sum(aaf))

			lm_x(7)=aimag(0.25*sum(wwf))
			lm_x(8)=aimag(0.25*sum(aaf))
		!	end if
		end if

!		lmftol = 1.0e-14
!		lmgtol = 1.0e-12
!		lmxtol = 1.0e-16
		lmftol = 1.0e-12
		lmgtol = 1.0e-12
		lmxtol = 1.0e-12
		lmmaxfev = 10000000
		lmepsfcn=1.0e-16
		lmfactor=100.0
		call lmdif(least_squares,8,8,lm_x,lmfvec,lmftol,lmxtol,lmgtol,lmmaxfev,lmepsfcn, &
                      lmdiag,lmmode,lmfactor,lmnprint,lminfo,lmnfev,lmfjac,lmldfjac, &
                      lmipvt,lmqtf,lmwa1,lmwa2,lmwa3,lmwa4)

	!computed new frequencies
		cc1(k1)=cmplx(lm_x(1),lm_x(5))
		cc2(k1)=cmplx(lm_x(2),lm_x(6))
		ww0(k1)=cmplx(lm_x(3),lm_x(7))
		kk0(k1)=cmplx(lm_x(4),lm_x(8))


		!use curve fit to get new k
		print *, '***info,fvec,w0,k0=',lminfo,ww0(k1),kk0(k1),lmfvec
		kkf=aaf
		err_w0 = abs(ww0(k1)-ww0(k1-1))
	if ((err_w0>rel_tol*1000.0) .OR. (k1<=1) .OR. ((k1>1) .AND. (minval(real(kkg))<=1.0e-2).AND. (minval(real(aaf))>=1.0e-2)))then
!print *, 'k1,k1g,err_w0,kkg,aaf=',k1,k1g,err_w0,minval(real(kkg)),minval(real(aaf))
			wwg=wwf
			kkg=aaf
			k1g = k1

		end if

		if (l1>=1) then
		print *, 'k1,err,tol=',k1,err_w0,rel_tol
		open(unit=21,file='kk0.out',position='append')
		open(unit=210,file='kkf.out',position='append')
		open(unit=220,file='wwf.out',position='append')

		open(unit=22,file='ww0.out',position='append')
		open(unit=23,file='cc.out',position='append')
		write(21,*) l1,real(kk0(k1)),aimag(kk0(k1))
		write(210,'(I4,8E28.16)') l1,real(kkf(1,1)),aimag(kkf(1,1)),real(kkf(2,1)),aimag(kkf(2,1)), &
						real(kkf(1,2)),aimag(kkf(1,2)),real(kkf(2,2)),aimag(kkf(2,2))
		write(22,'(I4,6E28.16)') l1,real(ww0(k1)),aimag(ww0(k1)),err_w0,sum(abs(lmfvec))
			write(220,'(I4,8E28.16)') l1,real(wwf(1,1)),aimag(wwf(1,1)),real(wwf(2,1)),aimag(wwf(2,1)), &
						real(wwf(1,2)),aimag(wwf(1,2)),real(wwf(2,2)),aimag(wwf(2,2))
		write(23,'(I4,4E28.16)') l1,real(cc1(k1)),aimag(cc1(k1)),real(cc2(k1)),aimag(cc2(k1))
		close(21)
		close(210)
		close(22)
		close(23)
		close(220)
	!write restart data

		if ((k1==1)) then
			open(unit=240,file='restart.dat',position='append')
			write(240,*) 'l1=',l1
			write(240,*) 'Re, theta=', Re, theta,ue
			write(240,*) 'lg,Sc,L=',lg,Sc,L_FENE
			write(240,*) kkf(1,1)
			write(240,*) kkf(2,1)
			write(240,*) kkf(1,2)
			write(240,*) kkf(2,2)
			write(240,*) wwf(1,1)
			write(240,*) wwf(2,1)
			write(240,*) wwf(1,2)
			write(240,*) wwf(2,2)
			close(240)
		end if
		end if


	end do !while k1


	if (l1 >=1) then
		wwfff(l1)=ww0(k1)
		aafff(l1)=kk0(k1)
		cc1fff(l1) = cc1(k1)
		cc2fff(l1) = cc2(k1)
	end if
end subroutine loop


!---------------------------------------------------------------------------------

	 subroutine OS_integrate(nx,x,fvec,iflag)
		use variables
		use parameters
		use dlsode_variables
		use grid
		implicit none
	        integer :: nx,iflag,i1,j1,k1,k10
		double precision :: x(nx),fvec(nx),za,zb
		real*8 :: YSTART(NEQ,NEQ/4),angle(NEQ/4*(NEQ/4-1)/2)
		real*8, parameter :: h1=0.0001,hmin=1.e-14,eps=1.e-12
		complex*16 :: F1(NEQ/2,NEQ/4),F2(NEQ/2,NEQ/4),det_matrix(Neq/2,Neq/2), det,determinant,gamma
		real*8, parameter :: angle_threshold=5.0
		external Derivs,RKQS,jacobian

!		print *, 'enter OS_integrate'

		aa=cmplx(x(1),x(2))
		kk2 = aa*aa


		!integrate OS_equation twice with different initial conditions
		!integrate with subroutine Derivs

		!*GENERATE INITIAL CONDITIONS
		YSTART=0.0
		call mean_flow(mean_type,r0,theta,Ue,U,dU,d2U,d3U)
		gamma=ii*(aa*U-ww)
	    bb=sqrt(aa**2+Re*gamma)
		call inner_conditions(NEQ,r0,aa,bb,gamma,Re,U,lg,Sc,YSTART,flag_symmetry)
	if ((mean_type == 5) .and. (r0>0.0))   call inner_conditions(NEQ,r0,aa,bb,gamma,Re,U,lg,Sc,YSTART,flag_symmetry+200)
        F1 =  cmplx(YSTART(1:NEQ/2,:),YSTART(NEQ/2+1:NEQ,:))
		do k1=1,NI-1
			do j1=1,NEQ/4
				ISTATE=1
				za=ZI(k1)
				zb=ZI(k1+1)
!!			print *, 'j1, YSTART=', j1, YSTART(:,j1)
!!			print *, 'za,zb=', ZI(k1),ZI(k1+1)
				rwork=0.0
				iwork=0
				iwork(6)=50000
				call DLSODE(Derivs,NEQ,YSTART(:,j1),za,zb,1,rtol,atol,1,istate,0,rwork,lrw,iwork,liw,jacobian,mf)
				do i1=1,NEQ/2
					F1(i1,j1)=cmplx(YSTART(i1,j1),YSTART(i1+NEQ/2,j1))
				end do

!!			print *, 'ISTATE=',ISTATE
!!			print *, 'YSTART=',YSTART(:,j1)
		end do

		if (flag_orthoI==1) then
		!*CHECK ANGLE AND ORTHOGONALIZE AS NEEDED
		call calculate_angle(NEQ,F1,angle)
		if (flag_display==10) print *, 'r,angle=',zI(k1),minval(abs(angle)),maxval(abs(F1))
	!	print *, 'angle=',minval(abs(angle))
		if ((minval(abs(angle))<angle_threshold) .OR. (k1==NI-1) .OR. (maxval(abs(F1))>1.0e4)) then
			if (flag_display==1) 	print *, '***ORTHOGONALIZE***,r=',ZI(k1),minval(abs(angle)),maxval(abs(F1))
			j_orthoI(k1)=1
			call orthogonalize(NEQ,P_orthoI(:,:,k1),F1)

			if (k1==NI-1) then
!				flag_orthoI=2
			end if
		end if
		elseif ((flag_orthoI==2) .AND. (j_orthoI(k1)==1)) then
!!			print *, '***ORTHOGONALIZE***, r=',ZI(k1)
			F1=matmul(F1,P_orthoI(:,:,k1))
		end if

		!*SAVE ORTHOGONALIZATION FACTORS/MATRICES


			YSTART(1:NEQ/2,:)=real(F1)
		    YSTART(NEQ/2+1:NEQ,:)=aimag(F1)
	end do

		!*GENERATE OUTER CONDITIONS
		call mean_flow(mean_type,rf,theta,Ue,U,dU,d2U,d3U)
		gamma=ii*(aa*U-ww)
		bb = -gamma*Re - 2.0*aa*aa
		kk=aa**4 + aa*aa*gamma*Re
		call outer_conditions(NEQ,mean_type,rf,aa,bb,gamma,kk,Re,U,lg,Sc,F2)
!		print *, 'outer conditions:',F2

                 k10=1
                 if (mean_type==500) then
                        do k1=1,NO-1
                                call mean_flow(mean_type,zO(k1),theta,Ue,U,dU,d2U,d3U)
                                if (abs(dU) < 1.0e-10) then
                                        k10 = k1
                                end if
                        end do

                        call mean_flow(mean_type,zO(k10),theta,Ue,U,dU,d2U,d3U)
                        gamma=ii*(aa*U-ww)
                        bb = -gamma*Re - 2.0*aa*aa
                        kk=aa**4 + aa*aa*gamma*Re
                        call outer_conditions(NEQ,mean_type,zO(k10),aa,bb,gamma,kk,Re,U,lg,Sc,F2)
                end if


		YSTART(1:NEQ/2,:)=real(F2)
		YSTART(NEQ/2+1:NEQ,:)=aimag(F2)
		do k1=k10,NO-1
			do j1=1,NEQ/4
				ISTATE=1
				za=ZO(k1)
				zb=ZO(k1+1)
		!	print *, 'j1, YSTART=', j1, YSTART(:,j1)
		!	print *, 'za,zb=', Z(k1),Z(k1+1)
				rwork=0.0
				iwork=0
				iwork(6)=50000

				call DLSODE(Derivs,NEQ,YSTART(:,j1),za,zb,1,rtol,atol,1,istate,0,rwork,lrw,iwork,liw,jacobian,mf)

				do i1=1,NEQ/2
					F2(i1,j1)=cmplx(YSTART(i1,j1),YSTART(i1+NEQ/2,j1))
				end do

		!	print *, 'ISTATE=',ISTATE
		!	print *, 'YSTART=',YSTART(:,j1)

		end do

		if (flag_orthoO==1) then
		!*CHECK ANGLE AND ORTHOGONALIZE AS NEEDED
		call calculate_angle(NEQ,F2,angle)
if (flag_display==10) 		print *, 'r,angle=',zO(k1),minval(abs(angle)),maxval(abs(F2))
		if ((minval(abs(angle))<angle_threshold) .OR. (k1==NO-1) .OR. (maxval(abs(F2))>1.0e4)) then
	if (flag_display==1) 		print *, '***ORTHOGONALIZE0***,r=',ZO(k1), minval(abs(angle)),maxval(abs(F2(:,2)))
			j_orthoO(k1)=1
			call orthogonalize(NEQ,P_orthoO(:,:,k1),F2)

			if (k1==NO-1) then
!				flag_orthoO=2
			end if
		end if
		elseif ((flag_orthoO==2) .AND. (j_orthoO(k1)==1)) then
!!			print *, '***ORTHOGONALIZE***, r=',ZO(k1)
			F2=matmul(F2,P_orthoO(:,:,k1))
		end if

		!*SAVE ORTHOGONALIZATION FACTORS/MATRICES


			YSTART(1:NEQ/2,:)=real(F2)
		    YSTART(NEQ/2+1:NEQ,:)=aimag(F2)
	end do



		!Compute determinant using results from integration
		!*MODIFY DETERMINANT MATRIX APPROPRIATELY
		det_matrix(:,1:2)=F1
		det_matrix(:,3:4)=-F2

	!	det_matrix(1,:)=(/1.0, 2.0, 3.0, 4.0/)
!		det_matrix(2,:)=0.5*det_matrix(1,:)
!!		det_matrix(4,:)=(/1.0, 0.0, 2.0, 0.0/)
		det=determinant(4,det_matrix)
!		print *, 'det_matrix='
if (1==2) then
		open(unit=22,file='detr.out')
		open(unit=23,file='deti.out')
		do i1=1,NEQ/2
		write(22,('(1000E28.16)')), real(det_matrix(i1,:))
				write(23,('(1000E28.16)')), aimag(det_matrix(i1,:))

		print *, (det_matrix(i1,:))
		end do
		close(22)
		close(23)
		print *, 'det=',det
end if
		fvec(1)=real(det)
		fvec(2)=aimag(det)

!		fvec(1) = x(1)**2-pi
!		fvec(2)=0.0
!!!	print *, 'aa,fvec=',aa,fvec
!!!	print *, 'ue,lg,sc,Re=',ue,lg,sc,Re
!!!	print *, 'det_matrix=',det_matrix

!!			print *, 'exit OS_integrate'
	!	STOP
	end subroutine OS_integrate
!---------------------------------------------------------------------------------


!---------------------------------------------------------------------------------
subroutine Derivs(NEQ,x,Y,DYDX)
	use variables
	use polymer_variables
	use parameters
	implicit none
	integer :: NEQ,i1,j1
	double precision :: x, Y(*),DYDX(*)
	complex*16 :: MM(NEQ/2,NEQ/2),NNI(NEQ/2,NEQ/2),RHS(NEQ/2),f(NEQ/2),gamma
	complex*16 :: R1g,R2g,R3g,R4g,S1g,S2g,S3g,S4g,invphi,RHSg(NEQ/2)
	real*8 :: MD(8)
!	print *, 'enter derivs'

	do i1=1,NEQ/2
		f(i1)=cmplx(Y(i1),Y(i1+NEQ/2))
	end do
	call mean_flow(mean_type,x,theta,Ue,U,dU,d2U,d3U)
	call polymer_mean(lg,sg,Sc,U,dU,d2U,d3U)

	gamma=ii*(aa*U-ww)
!	call giesekus_model(gamma)
!!!	if (abs(x)<1) then
!		print *, 'R=',R1,R2,R3,R4
!		print *, 'S=',S1,S2,S3,S4
	if (model_type==1) then
		call oldroyd_model(gamma)
	elseif (model_type==2) then
		call giesekus_model(gamma)
	elseif (model_type==3) then
		call FENE_model2(gamma)
	end if
if (1==2) then
	print *, 'oldroyd:', R1,R2,R3,R4
	print *, S1,S2,S3,S4
	call giesekus_model(gamma)
	print *, 'giesekus:', R1,R2,R3,R4
	print *, S1,S2,S3,S4
	STOP
end if
MM=0.0
	MM(1,2) = 1.0
	MM(2,1) = gamma*Re+kk2*Sc-R3*(1.0-Sc)
	MM(2,2) = -(1.0-Sc)*R2
	MM(2,3) = Re*dU-(1.0-Sc)*R4
	MM(2,4) = ii*aa*Re
	MM(3,1) = -ii*aa
	MM(4,1) = (1.0-Sc)*S3
	MM(4,2) = (1.0-Sc)*S2 - ii*aa*Sc

	MM(4,3) = -gamma*Re-kk2*Sc+(1.0-Sc)*S4
!!	print *, 'kk2=',kk2
!!	print *, 'MM=',MM

!!	MM=0.0
	NNI=0.0
!!	MM(1,2) = 1.0
!!	MM(2,1) = gamma*Re+kk2

!!	MM(2,3) = Re*(dU)
!!	MM(2,4) = ii*aa*Re
!!	MM(3,1) = -ii*aa
!!	MM(4,2) = -ii*aa/Re

!!	MM(4,3) = -gamma-kk2/Re

	NNI(1,1) = 1.0
	NNI(2,2) = 1.0/((1.0-Sc)*R1+Sc)
	NNI(3,3) = 1.0
	NNI(4,2) = ((1.0-Sc)*S1)/(Re*((1.0-Sc)*R1)+Sc)
	NNI(4,4) = 1.0/Re

	MM = matmul(NNI,MM)
	RHS=matmul(MM,f)



!!	print *, 'RHS=',RHS
!!	STOP
!	if (abs(x)<1) print *, 'RHS=',RHS
	do i1=1,NEQ/2
		DYDX(i1)=real(RHS(i1))
		DYDX(i1+NEQ/2)=aimag(RHS(i1))
	end do
end subroutine Derivs
!---------------------------------------------------------------------------------

!---------------------------------------------------------------------------------

	SUBROUTINE make_grid(NI,NO,r0,rm,rf,ZI,ZO,read_grid)
	use parameters
	implicit none
	integer :: NI,NO,i1,read_grid,N
	real*8 :: ZI(NI),ZO(NO),r0,rm,rf

	if (read_grid==1) then
	N=NI+NO-1
		open(unit=23,file='z.out')
		do i1=1,NI
			read(23,*) ZI(i1)
		end do
		do i1=2,NO
			read(23,*) ZO(NO+1-i1)
		end do
		ZO(NO) = zI(NI)
	close(23)
	else

	do i1=1,NI
		ZI(i1)=r0+real(i1-1)*(rm-r0)/real(NI-1)
!			T(i1)=pi*real(i1-1)/real(N-1)
	end do
	do i1=1,NO
		ZO(i1)=rf+real(i1-1)*(rm-rf)/real(NO-1)
!			T(i1)=pi*real(i1-1)/real(N-1)
	end do
!	Z=cos(T)
	end if



	end subroutine make_grid
!---------------------------------------------------------------------------------

!---------------------------------------------------------------------------------

subroutine jacobian()
end subroutine jacobian
!---------------------------------------------------------------------------------
subroutine least_squares(m,n,x,fvec,iflag) !function called by lmdif for nonlinear least squares problem
	use variables
	implicit none
	integer :: m,n,iflag
	double precision :: x(n),fvec(m)
	complex*16 :: a1,a2,w0,k0,w1,w2,w3,w4,k1p,k2p,k1m,k2m,func(4)
	w1=wwf(1,1)
	w2=wwf(2,1)
	w3=wwf(1,2)
	w4=wwf(2,2)
	k1p=aaf(1,1)
	k1m=aaf(1,2)
	k2p=aaf(2,1)
	k2m=aaf(2,2)

	a1=cmplx(x(1),x(5))
	a2=cmplx(x(2),x(6))
	w0=cmplx(x(3),x(7))
	k0=cmplx(x(4),x(8))

if (1==2) then
	print *, 'lmdif'
	print *, 'w1,w2=',w1,w2
	print *, 'k1p,k1m,k2p,k2m=',k1p,k1m,k2p,k2m
	print *, 'w0,k0=',w0,k0
	print *, 'a1,a2=',a1,a2
end if

	func(1) = k1p - k0 + a1*sqrt(w1-w0) - a2*(w1-w0)
	func(2) = k2p - k0 + a1*sqrt(w2-w0) - a2*(w2-w0)
	func(3) = k1m - k0 - a1*sqrt(w3-w0) - a2*(w3-w0)
	func(4) = k2m - k0 - a1*sqrt(w4-w0) - a2*(w4-w0)

	print *, 'func=',func

	fvec(1:4)=real(func)
	fvec(5:8)=aimag(func)
end subroutine least_squares

!---------------------------------------------------------------------------------
subroutine evaluate_k(a1,a2,w0,k0,w1,w2,kkf)
	implicit none
	complex*16 :: a1,a2,w0,k0,w1,w2,kkf(2,2)
	kkf(1,1) =  k0 - a1*sqrt(w1-w0) + a2*(w1-w0)
	kkf(1,2) =  k0 + a1*sqrt(w2-w0) + a2*(w2-w0)
	kkf(2,1) =  k0 - a1*sqrt(w2-w0) + a2*(w2-w0)
	kkf(2,2) =  k0 + a1*sqrt(w1-w0) + a2*(w1-w0)
end subroutine evaluate_k

!---------------------------------------------------------------------------------
