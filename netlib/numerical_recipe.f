************************************************************************
*  Subroutine: ODE_INT                                                 *
*                                                                      *
*  Function:   It is a Runge-Kutta driver with adaptive step size      *
*              control.  Integrate the Nvar starting values YSTART     *
*              from x1 to x2 with accuracy eps.  h1 should be set as a *
*              guessed first step size, hmin as the minimum allowed    *
*              step size (can be zero).  YSTART is replaced by values  *
*              at the end of the integration interval.  Derivs is the  *
*              user-supplied subroutine for calculating the right-hand *
*              side derivative, while RKQS is the name of the stepper  *
*              routine to be used.                                     *
************************************************************************
      Subroutine ODE_INT(YSTART, Nvar, x1, x2, eps, h1, hmin, Derivs, 
     .                   RKQS)


      Integer maxstp, Nmax, Nvar

      Parameter(maxstp = 1000000, Nmax = 100)

      Integer I, nstp
      Real*8  Ystart(Nvar), x1, x2, eps, h1, hmin
      Real*8  x, h, Y(Nmax), DYDX(Nmax), hdid, hnext


      External Derivs, RKQS

      x = x1 
      h = dsign(h1, x2 - x1)

      Do I = 1, Nvar 
        Y(I) = Ystart(I) 
      End Do

*	print *, 'enter ODE_INT, ,x1,x2,eps=',x1,x2,eps
*       print *, 'ystart,Nvar,h1,hmin=',YSTART,Nvar, h1,hmin
*  Take at most MAXSTP steps
      Do nstp = 1, maxstp 
        Call Derivs(x, Y, DYDX)

*     If step can overshoot, decrease.
        If ((x + h - x2) * (x + h - x1) .GT. 0.0D0) h = x2 - x 
        Call RKQS(Y, DYDX, Nvar, x, h, eps, hdid, hnext, Derivs)

*     Are we done? 
        If ((x - x2) * (x2 - x1) .GE. 0.0D0) Then
          Do I = 1, Nvar
            YSTART(I) = Y(I)
          End Do

*         Normal exit 
          Return

        End If

        If (dabs(hnext) .LT. hmin) 
     .    Stop 'ODE_INT: Step size smaller than minimum!'

        h = hnext 
      End Do

      Stop 'ODE_INT: Too many steps!'

      End


************************************************************************
*  Subroutine: RKQS                                                    *
*                                                                      *
*  Function:   It is a fifth-order Runge-Kutta stepper routine with    *
*              monitoring of local truncation error to ensure accuracy *
*              and adjust stepsize.  Input are the dependent variable  *
*              vector Y of length N and its derivative DYDX at the     *
*              starting value of the independent variable x.  Also     *
*              input are the stepsize to be attempted htry, the        *
*              required accuracy eps, and the vector YSCAL against     *
*              which the error is scaled.  On output, Y and x are      *
*              replaced by their new values, hdid is the stepsize      *
*              which was actually accomplished, and hnext is the       *
*              estimated next stepsize.  Derivs is the user-supplied   *
*              subroutine that computes the right-hand side            *
*              derivatives.                                            *
************************************************************************
      Subroutine RKQS(Y, DYDX, N, x, htry, eps, hdid, hnext, Derivs)

      Integer N, Nmax

      Real*8  Y(N), DYDX(N), x, htry, eps, hdid, hnext

      Parameter(Nmax = 100)

      Integer I
      Real*8  Yerr(Nmax), Ytemp(Nmax)
      Real*8  safety, pgrow, pshrnk, errcon, h, errmax, hr, xnew

      Parameter(safety = 9.0D-1,  pgrow  = -2.0D-1)
      Parameter(pshrnk = -2.5D-1, errcon = 1.89D-4)

      External Derivs

*  Set step size to the initial trial value 
      h = htry 

*  Take a step 
 1    Call RKCK(Y, DYDX, N, x, h, Ytemp, Yerr, Derivs)

*  Evaluate accuracy 
      errmax = 0.0D0

      Do I = 1, N
        errmax = dmax1(errmax, dabs(Yerr(I) / Ytemp(I)))
      End Do

*  Scale relative to required tolerance
      errmax = errmax / eps 

*  Truncation error too large, reduce stepsize 
      If (errmax .GT. 1.0D0) Then
        hr = safety * h * (errmax ** pshrnk)

*     No more than a factor of 10
        If (dabs(hr) .LT. 1.0D-1 * dabs(h)) Then 
          hr = 1.0D-1 * h
        End If 

        h    = hr 
        xnew = x + h 
        If (xnew .EQ. x) Stop 'RKQS: Stepsize underflow!'

*       For another try 
        Go To 1

*     Step succeeded.  Compute size of next step
      Else 

        If (errmax .GT. errcon) Then 
          hnext = safety * h * (errmax ** pgrow)
*       No more than a factor of 5 increase
        Else 
          hnext = 5.0D0 * h
        End If 

        hdid = h 
        x = x + h

        Do I = 1, N 
          Y(I) = Ytemp(I)
        End Do

        Return
      End If 

      End 


************************************************************************
*  Subroutine: RKCK                                                    *
*                                                                      *
*  Function:   Given values for N variables Y and their derivatives    *
*              DYDX known at x, use the fifth-order Cash-Karp          *
*              Runge-Kutta method to advance the solution over an      *
*              interval h and return the incremented variables as      *
*              Yout.  Also return an estimate of the local truncation  *
*              error in Yout using the embedded fourth-order method.   *
*              The user supplies the subroutine Derivs(x, Y, DYDX),    *
*              which returns derivatives DYDX at x.                    *
************************************************************************
      Subroutine RKCK(Y, DYDX, N, x, h, Yout, Yerr, Derivs)

      Integer N, Nmax
      Real*8  DYDX(N), Y(N), x, h, Yout(N), Yerr(N)

      External Derivs 

      Parameter(Nmax = 100)

      Integer I
      Real*8  Ak2(Nmax), Ak3(Nmax), Ak4(Nmax), Ak5(Nmax)
      Real*8  Ak6(Nmax), Ytemp(Nmax)
      Real*8  A2, A3, A4, A5, A6
      Real*8  B21
      Real*8  B31, B32
      Real*8  B41, B42, B43
      Real*8  B51, B52, B53, B54
      Real*8  B61, B62, B63, B64, B65
      Real*8  C1,  C3,  C4,  C6
      Real*8  DC1, DC3, DC4, DC5, DC6

      Parameter(A2=2.0D-1, A3=3.0D-1, A4=6.0D-1, A5=1.0D0, A6=8.75D-1)
      Parameter(B21=2.0D-1)
      Parameter(B31=3.0D0/4.0D1, B32=9.0D0/4.0D1)
      Parameter(B41=3.0D-1, B42=-9.0D-1, B43=1.2D0)
      Parameter(B51=-1.1D1/5.4D1, B52=2.5D0, B53=-7.0D1/2.7D1, 
     .          B54=3.5D1/2.7D1)
      Parameter(B61=1.631D3/5.5296D4, B62=1.75D2/5.12D2, 
     .          B63=5.75D2/1.3824D4,  B64=4.4275D4/1.10592D5, 
     .          B65=2.53D2/4.096D3)
      Parameter(C1=3.7D1/3.78D2, C3=2.50D2/6.21D2, C4=1.25D2/5.94D2, 
     .          C6=5.12D2/1.771D3)
      Parameter(DC1=C1-2.825D3/2.7648D4,  DC3=C3-1.8575D4/4.8384D4, 
     .          DC4=C4-1.3525D4/5.5296D4, DC5=-2.77D2/1.4336D4, 
     .          DC6=C6-2.5D-1)

*  First step 
      Do I = 1, N
        Ytemp(I) = Y(I) + B21 * h * DYDX(I)
      End Do

*  Second step 
      Call Derivs(x+A2*h, Ytemp, Ak2)
      Do I = 1, N
        Ytemp(I) = Y(I) + h * (B31 * DYDX(I) + B32 * Ak2(I)) 
      End Do

*  Third step 
      Call Derivs(x+A3*h, Ytemp, Ak3)
      Do I = 1, N 
        Ytemp(I) = Y(I) + h * ( B41 * DYDX(I) + B42 * Ak2(I) 
     .                        + B43 * Ak3(I) )
      End Do

*  Fourth step 
      Call Derivs(x+A4*h, Ytemp, Ak4)
      Do I = 1, N 
        Ytemp(I) = Y(I) + h * ( B51 * DYDX(I) + B52 * Ak2(I) 
     .                        + B53 * Ak3(I)  + B54 * Ak4(I) )
      End Do

*  Fifth step 
      Call Derivs(x+A5*h, Ytemp, Ak5)
      Do I = 1, N 
        Ytemp(I) = Y(I) + h * ( B61 * DYDX(I) + B62 * Ak2(I) 
     .                        + B63 * Ak3(I)  + B64 * Ak4(I) 
     .                        + B65 * Ak5(I) )
      End Do

*  Sixth step 
      Call Derivs(x+A6*h, Ytemp, Ak6)

*  Accumulate increments with proper wieghts 
      Do I = 1, N 
        Yout(I) = Y(I) + h * ( C1 * DYDX(I) + C3 * Ak3(I) 
     .                       + C4 * Ak4(I)  + C6 * Ak6(I) )
      End Do

      Do I = 1, N 
*       Estimate error as difference between fourth and fifth order 
*       methods
        Yerr(I) = h * ( DC1 * DYDX(I) + DC3 * Ak3(I) + DC4 * Ak4(I) 
     .                + DC5 * Ak5(I)  + DC6 * Ak6(I) )
      End Do

      Return 

      End


************************************************************************
*  Subroutine: LUDCMP                                                  *
*                                                                      *
*  Function:   Given a matrix A(n,n), with physical dimension np by    *
*              np,this routine replaces it by the LU decomposition of  *
*              a rowwise permutation of itself.  A and n are input.  A *
*              is output; indx(n) is an output vector that records the *
*              row permutation effected by the partial pivoting; d is  *
*              output as +/- 1 depending on whether the number of row  *
*              interchanges was even or odd, respectively.  This       *
*              routine is used in combination with LUBKSB to solve     *
*              linear equations or invert a matrix.                    *
************************************************************************
      Subroutine LUDCMP(A, n, np, indx, d)

      Integer n, np, indx(n), nmax
      Real*8  A(np,np), d, tiny

      Parameter (nmax = 500, tiny = 1.0D-20)

*  Locals
      Integer I, imax, J, K

*  vv stores the implicit scaling of each row
      Real*8  aamax, dum, sum, vv(nmax)

*  No row interchange yet
      d = 1.0D0

*  Loop over rows to get the implicit scaling information
      Do I = 1, n
        aamax = 0.0D0

        Do J = 1, n
          If (dabs(A(I,J)) .GT. aamax) aamax = dabs(A(I,J))
        End Do

        If (aamax .EQ. 0.0D0) Stop 'LUDCMP: Singular matrix'

*  Save the scaling
        vv(I) = 1.0D0 / aamax
      End Do

*  This is the loop over columns of Crout's method
      Do J = 1, n

        Do I = 1, J-1
          sum = A(I,J)

          Do K = 1, I-1
            sum = sum - A(I,K) * A(K,J)
          End Do

          A(I,J) = sum
        End Do

*  Initialize for the search for largest pivot element
        aamax = 0.0D0

        Do I = J, n
          sum = A(I,J)

          Do K = 1, J-1
            sum = sum - A(I,K) * A(K,J)
          End Do

          A(I,J) = sum

*  Figure of merit for the pivot
          dum = vv(I) * dabs(sum)

*  Is it better than the best so far?
          If (dum .GE. aamax) Then
            imax = I
            aamax = dum
          End If
        End Do

*  Do we need to interchange rows?
        If (J .NE. imax) Then

*  Yes, do so
          Do K = 1, n
            dum = A(imax,K)
            A(imax,K) = A(J,K)
            A(J,K) = dum
          End Do

*  and change the parity of d
          d = - d

*  Also interchange the scale factor
          vv(imax) = vv(J)

        End If

        indx(J) = imax

        If (A(J,J) .EQ. 0.0D0) A(J,J) = tiny

*  If the pivot element is zero, the matrix is singular (at least to the 
*  precision of the algorithm).  For some applications on singular 
*  matrices, it is desirable to substitute tiny for zero.

*  Now, finally divide by the pivot element
        If (J .NE. n) Then
          dum = 1.0D0 / A(J,J)

          Do I = J+1, n
            A(I,J) = A(I,J) * dum
          End Do

        End If

*  Go back for the next column in the reduction
      End Do

      Return

      End


************************************************************************
*  Subroutine: LUBKSB                                                  *
*                                                                      *
*  Function:   It solves the set of linear equations A.X = B.  Here A  *
*              is input, not as the matrix A but rather as its LU      *
*              decomposition, determined by the routine LUDCMP.  indx  *
*              is input as the permutation vector returned by LUDCMP.  *
*              b(n) is input as the right-hand side vector b, and      *
*              returns with the solution vector X.  A, n, np, and indx *
*              are not modified by this routine and can be left in     *
*              place for successive calls with different right-hand    *
*              sides b.  This routine takes into account the           *
*              possibility that b will begin with many zero elements,  *
*              so it is efficient for use in matrix inversion.         *
************************************************************************
      Subroutine LUBKSB(A, n, np, indx, b)

      Integer n, np, indx(n)
      Real*8  A(np,np), b(n)

*  Locals
      Integer I, II, J, LL
      Real*8  sum

*  When II is set to a positive value, it will become the index of the 
*  first nonvanishing element of b.  We now do the forward 
*  substitution.  The only new wrinkle is to unscramble the permutation 
*  as we go.
      II = 0

      Do I = 1, n
        LL = indx(I)
        sum = b(LL)
        b(LL) = b(I)

        If (II .NE. 0) Then
          Do J = II, I-1
            sum = sum - A(I,J) * b(J)
          End Do

        Else If (sum .NE. 0.0D0) Then
*  A non-zero element was encountered, so from now on we will have to 
*  do the sums in the loop above
          II = I

        End If

        b(I) = sum
      End Do

*  Now we do the back substitution
      Do I = n, 1, -1
        sum = b(I)

        Do J = I+1, n
          sum = sum - A(I,J) * b(J)
        End Do

*  Store a component of the solution vector X
        b(I) = sum / A(I,I)
      End Do

*  All done!
      Return

      End


************************************************************************
*  Subroutine: SPLINE                                                  *
*                                                                      *
*  Function:   Given arrays x(N) and y(N) containing a tabulated       *
*              function, i.e. y_i = f(x_i), with x_1 < x_2 < ... < x_N *
*              and given values yp1 and ypn for the first derivative   *
*              of the interpolating function at points 1 and N,        *
*              respectively, this routine returns an array y2(N) of    *
*              length N which contains the second derivatives of the   *
*              interpolating function at the tabulated points x_i.  If *
*              yp1 and/or ypn are equal to 10^(30) or larger, the      *
*              routine is signaled to set the corresponding boudary    *
*              condition for a natural spline, with zero second        *
*              derivative on that boundary.                            *
************************************************************************
      Subroutine SPLINE(x, y, N, yp1, ypn, y2)

      Integer N, Nmax
      Real*8  yp1, ypn, x(N), y(N), y2(N)

*  Set the largest anticipated value of N
      Parameter (Nmax = 2000000)

*  Locals
      Integer I, K
      Real*8  p, qn, sig, un, u(Nmax)

*  The lower boundary condition is set either to be "natural"
      If (yp1 .GT. 9.9D29) Then
        y2(1) = 0.0D0
        u(1)  = 0.0D0

*  or else to have a specified first derivative.
      Else
        y2(1) = -5.0D-1
        u(1)  = 3.0D0 / (x(2) - x(1)) * ( (y(2) - y(1)) / (x(2) - x(1))
     .            - yp1 )
      End If

*  This is the decomposition loop of the tridiagonal algorithm.  y2 and 
*  u are used for temporary storage of the decomposed factors.
      Do I = 2, N-1
        sig   = (x(I) - x(I-1)) / (x(I+1) - x(I-1))
        p     = sig * y2(I-1) + 2.0D0
        y2(I) = (sig - 1.0D0) / p
        u(I)  = ( 6.0D0 * ( (y(I+1) - y(I)) / (x(I+1) - x(I)) 
     .            - (y(I) - y(I-1)) / (x(I) - x(I-1)) ) / 
     .            (x(I+1) - x(I-1)) - sig * u(I-1) ) / p
      End Do

*  The upper boundary condition is set either to be "natural"
      If (ypn .GT. 9.9D29) Then
        qn = 0.0D0
        un = 0.0D0

*  or else to have a specified first derivative.
      Else
        qn = 5.0D-1
        un = 3.0D0 / (x(N) - x(N-1)) * ( ypn - (y(N) - y(N-1)) / 
     .         (x(N) - x(N-1)) )
      End If

      y2(N) = (un - qn * u(N-1)) / (qn * y2(N-1) + 1.0D0)

*  This is the back-substitution loop of the tridiagonal algorithm.
      Do K = N-1, 1, -1
        y2(K) = y2(K) * y2(K+1) + u(K)
      End Do

      Return

      End


************************************************************************
*  Subroutine: SPLINT                                                  *
*                                                                      *
*  Function:   Given the arrays xa(N) and ya(N) of length N, which     *
*              tabulate a function (with the xa_i's in order), and     *
*              given the array y2a(N), which is the output from SPLINE *
*              above, and given a value of x, this routine returns a   *
*              cubic-spline interpolated value y.                      *
************************************************************************
      Subroutine SPLINT(xa, ya, y2a, N, x, y)

      Integer N
      Real*8  x, y, xa(N), y2a(N), ya(N)

*  Locals
      Integer K, khi, klo
      Real*8  a, b, h

*  We will find the right place in the table by means of bisection.
      klo = 1
      khi = N

 1    If (khi - klo .GT. 1) Then
        K = (khi + klo) / 2

        If (xa(K) .GT. x) Then
          khi = K
        Else
          klo = K
        End If

        Go To 1

      End If

*  klo and khi now bracket the input value of x.
      h = xa(khi) - xa(klo)

*  The xa's must be distinct.
      If (h .EQ. 0.0D0) Stop 'SPLINT: Bad xa input!'

*  Cubic spline polynomial is now evaluated.
      a = (xa(khi) - x) / h
      b = (x - xa(klo)) / h

      y = a * ya(klo) + b * ya(khi) + 
     .      ( (a * a * a - a) * y2a(klo) + (b * b * b - b) * y2a(khi) )
     .        * h * h / 6.0D0

      Return

      End

************************************************************************
*  Subroutine: CSPLINT                                                  *
*                                                                      *
*  Function:   Complex version of SPLINT above. Given the arrays xa(N) and ya(N) of length N, which     *
*              tabulate a function (with the xa_i's in order), and     *
*              given the array y2a(N), which is the output from SPLINE *
*              above, and given a value of x, this routine returns a   *
*              cubic-spline interpolated value y.                      *
************************************************************************
      Subroutine CSPLINT(xa, ya, y2a, N, x, y)

      Integer N
      Real*8  xa(N), y2a(N), ya(N)
      complex*16 :: x,y
*  Locals
      Integer K, khi, klo
      Real*8  h
      complex*16 :: a,b
*  We will find the right place in the table by means of bisection.
      klo = 1
      khi = N

 1    If (khi - klo .GT. 1) Then
        K = (khi + klo) / 2

        If (xa(K) .GT. real(x)) Then
          khi = K
        Else
          klo = K
        End If

        Go To 1

      End If

*  klo and khi now bracket the input value of x.
      h = xa(khi) - xa(klo)

*  The xa's must be distinct.
      If (h .EQ. 0.0D0) Stop 'SPLINT: Bad xa input!'

*  Cubic spline polynomial is now evaluated.
      a = (xa(khi) - x) / h
      b = (x - xa(klo)) / h

      y = a * ya(klo) + b * ya(khi) + 
     .      ( (a * a * a - a) * y2a(klo) + (b * b * b - b) * y2a(khi) )
     .        * h * h / 6.0D0

      Return

      End


************************************************************************
*  Subroutine: QROMB                                                   *
*                                                                      *
*  Function:   It returns as ss the integral of the function func from *
*              a to b.  Integration is performed by Romberg's method   *
*              of order 2K, where e.g. K = 2 is Simpson's rule.
************************************************************************
      Subroutine QROMB(func, a, b, ss)

      Integer Jmax, Jmaxp, K, Km
      Real*8  a, b, func, ss, eps

      External func

*  eps  - the fractional accuracy desired, as determined by the 
*         extrapolation error estimate
*  Jmax - limits the total number of steps.
*  K    - the number of points used in the extrapolation.
      Parameter (eps = 1.0D-7, Jmax = 20, Jmaxp = Jmax + 1)
      Parameter (K = 5, Km = K - 1)

*  Locals
      Integer J
*  These store the successive trapezoidal approximations and their 
*  relative step sizes.
      Real*8  dss, h(Jmaxp), s(Jmaxp)

      h(1) = 1.0D0

      Do J = 1, Jmax
        Call TRAPZD(func, a, b, s(J), J)

        If (J .GE. K) Then
          Call POLINT(h(J-Km), s(J-Km), K, 0.0D0, ss, dss)
          If (dabs(dss) .LE. eps * dabs(ss)) Return
        End If

        s(J+1) = s(J)

*  This is a key step: the factor is 0.25 even though the step size is 
*  decreased by only 0.5.  This makes the extrapolation a polynomial in 
*  h^2, not just a polynomial in h.
        h(J+1) = 2.5D-1 * h(J)
      End Do

      Stop 'QROMB: Too many steps!'

      End


************************************************************************
*  Subroutine: TRAPZD                                                  *
*                                                                      *
*  Function:   It computes the N-th stage of refinement of an extended *
*              trapezoidal rule.  func is input as the name of the     *
*              function to be integrated between limits a and b, also  *
*              input.  When called with N = 1, the routine returns as  *
*              s the crudest estimate of integral of f(x) from a to b. *
*              Subsequent calls with N = 2, 3, ... (in that sequential *
*              order) will improve the accuracy of s by adding 2^(N-2) *
*              additional interior points.  s should not be modified   *
*              between sequential calls.                               *
************************************************************************
      Subroutine TRAPZD(func, a, b, s, N)

      Integer N
      Real*8  a, b, s, func

      External func

*  Locals
      Integer it, J
      Real*8  del, sum, tnm, x

      If (N .EQ. 1) Then
        s = 5.0D-1 * (b - a) * (func(a) + func(b))

      Else
        it  = 2 ** (N-2)
        tnm = Dble(it)

*  This is the spacing of the points to be added.
        del = (b - a) / tnm
        x   = a + 5.0D-1 * del
        sum = 0.0D0

        Do J = 1, it
          sum = sum + func(x)
          x   = x + del
        End Do

*  This replaces s by its refined value.
        s = 5.0D-1 * (s + (b - a) * sum / tnm)
      End If

      Return

      End


************************************************************************
*  Subroutine: POLINT                                                  *
*                                                                      *
*  Function:   Given arrays xa and ya, each of length N, and given a   *
*              value x, this routine returns a value y, and an error   *
*              estimate dy.  If P(x) is the polynomial of degree N-1   *
*              such that P(xa_i) = ya_i, i = 1, ..., N, then the       *
*              returned value y = P(x).                                *
************************************************************************
      Subroutine POLINT(xa, ya, N, x, y, dy)

      Integer N, Nmax
      Real*8  dy, x, y, xa(N), ya(N)

*  Largest anticipated value of N
      Parameter (Nmax = 100)

*  Locals
      Integer I, M, ns
      Real*8  den, dif, dift, ho, hp, w, c(Nmax), d(Nmax)

      ns  = 1
      dif = dabs(x - xa(1))

* Here we find the index ns of the closest table entry,
      Do I = 1, N
        dift = dabs(x - xa(I))

        If (dift .LT. dif) Then
          ns  = I
          dif = dift
        End If

*  and initialize the tableau of c's and d's.
        c(I) = ya(I)
        d(I) = ya(I)
      End Do

*  This is the initial approximation to y.
      y  = ya(ns)
      ns = ns - 1

*  For each column of the tableau,
      Do M = 1, N-1

*  we loop over the current c's and d's and update them.
        Do I = 1, N-M
          ho  = xa(I) - x
          hp  = xa(I+M) - x
          w   = c(I+1) - d(I)
          den = ho - hp

*  This error can occur only if two input xa's are identical.
          If (den .EQ. 0.0D0) Stop 'POLINT: Problem!'

          den = w / den

*  Here the c's and d's are updated.
          d(I) = hp * den
          c(I) = ho * den
        End Do

*  After each column in the tableau is completed, we decide which 
*  correction, c or d, we want to add to our accumulating value of y, 
*  i.e. which path to take through the tableau - forking up or down.  
*  We do this in such a way as to take the most "straight line" route 
*  through the tableau to its apex, updating ns accordingly to keep 
*  track of where we are.  This route keeps the partial approximations 
*  centered on the target x.  The last dy added is thus the error 
*  indication.
        If (2*ns .LT. N-M) Then
          dy = c(ns+1)

        Else
          dy = d(ns)
          ns = ns - 1
        End If

        y = y + dy
      End Do

      Return

      End



************************************************************************
*  Subroutine: LUDCMP                                                  *
*                                                                      *
*  Function:   Given a matrix A(n,n), with physical dimension np by    *
*              np,this routine replaces it by the LU decomposition of  *
*              a rowwise permutation of itself.  A and n are input.  A *
*              is output; indx(n) is an output vector that records the *
*              row permutation effected by the partial pivoting; d is  *
*              output as +/- 1 depending on whether the number of row  *
*              interchanges was even or odd, respectively.  This       *
*              routine is used in combination with LUBKSB to solve     *
*              linear equations or invert a matrix.                    *
************************************************************************
      Subroutine CLUDCMP(A, n, np, indx, d)

      Integer n, np, indx(n), nmax
      complex*16  A(np,np), d, tiny

      Parameter (nmax = 500, tiny = 1.0D-20)

*  Locals
      Integer I, imax, J, K

*  vv stores the implicit scaling of each row
      complex*16  aamax, dum, sum, vv(nmax)

*  No row interchange yet
      d = 1.0D0

*  Loop over rows to get the implicit scaling information
      Do I = 1, n
        aamax = 0.0D0

        Do J = 1, n
          If (abs(A(I,J)) .GT. abs(aamax)) aamax = abs(A(I,J))
        End Do

        If (aamax .EQ. 0.0D0) Stop 'LUDCMP: Singular matrix'

*  Save the scaling
        vv(I) = 1.0D0 / aamax
      End Do

*  This is the loop over columns of Crout's method
      Do J = 1, n

        Do I = 1, J-1
          sum = A(I,J)

          Do K = 1, I-1
            sum = sum - A(I,K) * A(K,J)
          End Do

          A(I,J) = sum
        End Do

*  Initialize for the search for largest pivot element
        aamax = 0.0D0

        Do I = J, n
          sum = A(I,J)

          Do K = 1, J-1
            sum = sum - A(I,K) * A(K,J)
          End Do

          A(I,J) = sum

*  Figure of merit for the pivot
          dum = vv(I) * abs(sum)

*  Is it better than the best so far?
          If (abs(dum) .GE. abs(aamax)) Then
            imax = I
            aamax = dum
          End If
        End Do

*  Do we need to interchange rows?
        If (J .NE. imax) Then

*  Yes, do so
          Do K = 1, n
            dum = A(imax,K)
            A(imax,K) = A(J,K)
            A(J,K) = dum
          End Do

*  and change the parity of d
          d = - d

*  Also interchange the scale factor
          vv(imax) = vv(J)

        End If

        indx(J) = imax

        If (A(J,J) .EQ. 0.0D0) A(J,J) = tiny

*  If the pivot element is zero, the matrix is singular (at least to the 
*  precision of the algorithm).  For some applications on singular 
*  matrices, it is desirable to substitute tiny for zero.

*  Now, finally divide by the pivot element
        If (J .NE. n) Then
          dum = 1.0D0 / A(J,J)

          Do I = J+1, n
            A(I,J) = A(I,J) * dum
          End Do

        End If

*  Go back for the next column in the reduction
      End Do

      Return

      End


************************************************************************
*  Subroutine: LUBKSB                                                  *
*                                                                      *
*  Function:   It solves the set of linear equations A.X = B.  Here A  *
*              is input, not as the matrix A but rather as its LU      *
*              decomposition, determined by the routine LUDCMP.  indx  *
*              is input as the permutation vector returned by LUDCMP.  *
*              b(n) is input as the right-hand side vector b, and      *
*              returns with the solution vector X.  A, n, np, and indx *
*              are not modified by this routine and can be left in     *
*              place for successive calls with different right-hand    *
*              sides b.  This routine takes into account the           *
*              possibility that b will begin with many zero elements,  *
*              so it is efficient for use in matrix inversion.         *
************************************************************************
      Subroutine CLUBKSB(A, n, np, indx, b)

      Integer n, np, indx(n)
      complex*16  A(np,np), b(n)

*  Locals
      Integer I, II, J, LL
      complex*16  sum

*  When II is set to a positive value, it will become the index of the 
*  first nonvanishing element of b.  We now do the forward 
*  substitution.  The only new wrinkle is to unscramble the permutation 
*  as we go.
      II = 0

      Do I = 1, n
        LL = indx(I)
        sum = b(LL)
        b(LL) = b(I)

        If (II .NE. 0) Then
          Do J = II, I-1
            sum = sum - A(I,J) * b(J)
          End Do

        Else If (sum .NE. 0.0D0) Then
*  A non-zero element was encountered, so from now on we will have to 
*  do the sums in the loop above
          II = I

        End If

        b(I) = sum
      End Do

*  Now we do the back substitution
      Do I = n, 1, -1
        sum = b(I)

        Do J = I+1, n
          sum = sum - A(I,J) * b(J)
        End Do

*  Store a component of the solution vector X
        b(I) = sum / A(I,I)
      End Do

*  All done!
      Return

      End

