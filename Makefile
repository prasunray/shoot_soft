# Makefile for compiling viscoelastic shooting codes
# Assumes that source code for needed "external packages" are in $NETLIB_DIR (see below) and that
# odepack source files have been pre-compiled (see netlib/odepack/comp_odepack)

# fortran compiler and flags
FC= gfortran
FLAGS=  -fcheck=bounds -march=native -ffast-math -funroll-loops -fdefault-real-8 -fdefault-double-8 -O3 -ftree-vectorize
#FLAGS= -fdefault-real-8 -fdefault-double-8 -fcheck=all


# libraries
LIBS= -lblas -llapack -lodepack -lnetlib
LIB_DIR=./

# modules
MODS = dlsode_variables.mod  grid.mod  hybrd_variables.mod  lmdif_variables.mod  parameters.mod spline_variables.mod

# module 2
MODS2_FILES = variables.mod polymer_variables.mod

#netlib directory (minpack, odepack)
NETLIB_DIR= netlib/
NETLIB_FILES = ${NETLIB_DIR}minpack.f ${NETLIB_DIR}det44.f90 ${NETLIB_DIR}numerical_recipe.f
NETLIB_OFILES = minpack.o det44.o numerical_recipe.o

LDRIVER_DIR =

#source files
FILES = polymer_model.F90 bc.F90 mean_flow.F90 orthogonalize.F90 polymer_mean.F90
OBJS=$(FILES:.F90=.o)

FILES_RAYLEIGH = bcRayleigh.F90 mean_flow.F90 polymer_mean.F90
OBJS_RAYLEIGH=$(FILES_RAYLEIGH:.F90=.o)

#code for S_crit
shoot2absf_final: shoot2absf_final.F90 libodepack.a libnetlib.a modules.o shoot2absf_final.o ${OBJS}
	${FC} ${FLAGS} -o $@ shoot2absf_final.F90 ${OBJS} modules.o -L${LIB_DIR} ${LIBS}

#code for saddle point with S specified
shoot2abs_final: shoot2abs_final.F90 libodepack.a libnetlib.a modules.o shoot2abs_final.o ${OBJS}
	${FC} ${FLAGS} -o $@ shoot2abs_final.F90 ${OBJS} modules.o -L${LIB_DIR} ${LIBS}

#code for spatial problem with varying frequency
shoot2w_final:  shoot2w_final.F90 libodepack.a libnetlib.a modules.o shoot2w_final.o ${OBJS}
	@echo === $@
	${FC} ${FLAGS} -o $@ shoot2w_final.F90 ${OBJS} modules.o -L${LIB_DIR} ${LIBS}

#code for temporal problem with varying wavenumber
shoot2w_finalt: shoot2w_finalt.F90 libodepack.a libnetlib.a modules.o shoot2w_finalt.o ${OBJS}
	${FC} ${FLAGS} -o $@ shoot2w_finalt.F90 ${OBJS} modules.o -L${LIB_DIR} ${LIBS}

#elastic rayleigh analog to shoot2abs_final
erayleighabs_final : erayleighabs_final.F90 libodepack.a modules.o erayleighabs_final.o ${OBJS_RAYLEIGH}
	${FC} ${FLAGS} -o $@ $< modules.o ${OBJS_RAYLEIGH} -L${LIB_DIR} ${LIBS}

#maximum spatial growth rate, erayleigh eqn.
erayleigh_finals: erayleigh_finals.F90 libodepack.a modules.o erayleigh_finals.o ${OBJS_RAYLEIGH}
	${FC} ${FLAGS} -o $@ $< modules.o ${OBJS_RAYLEIGH} -L${LIB_DIR} ${LIBS}

#maximum temporal growth rate, erayleigh eqn.
erayleigh_finalt: erayleigh_finalt.F90 libodepack.a modules.o erayleigh_finalt.o ${OBJS_RAYLEIGH}
	${FC} ${FLAGS} -o $@ $< modules.o ${OBJS_RAYLEIGH} -L${LIB_DIR} ${LIBS}

libodepack.a: ${NETLIB_DIR}$/odepack/*.o ${NETLIB_DIR}$/odepack/*.f
	${FC} ${FLAGS} -c ${NETLIB_DIR}$/odepack/*.f
	ar cr libodepack.a ${NETLIB_DIR}$/odepack/*.o

libnetlib.a: ${NETLIB_FILES}
	${FC} ${FLAGS} -c ${NETLIB_FILES}
	ar cr libnetlib.a ${NETLIB_OFILES}


modules.o: modules.F90
	${FC} ${FLAGS} -c modules.F90 -o modules.o



.SUFFIXES: $(SUFFIXES) .F90 .f

.f.o:
	$(F77) $(FFLAG7) -c $<
.F90.o:
	$(FC) $(FLAGS) -c $<
