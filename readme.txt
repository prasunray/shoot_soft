This repo contains codes used to compute linear stability results presented in
Ray & Zaki (PoF 2014) and Ray & Zaki (PoF 2015). The eigenvalue problems are
solved using a shooting method with re-orthonormalization (see papers for references).

A Makefile is included which can be used to compile the codes using gfortran.
Note that the code in netlib/odepack needs to be "pre-compiled" prior to using
the makefile.

The folder, spatial_example, contains an input file set up to solve the spatial
stability problem for a planar FENE-P jet (see spatial_example/readme.txt).

Code:
bc.F90:
  These subroutines provide initial solutions for shooting method
  inner_conditions: initial solution for integration from r0 to rm
  outer_counditions: initial solution for integration from rf to rm

bcRayleigh.F90: Same as bc.F90 but for elastic Rayleigh equation

erayleigh_finals.F90: Maximum spatial growth rate, elastic Rayleigh eqn.

erayleigh_finalt.F90: Maximum temporal growth rate, elastic Rayleigh eqn.

erayleighabs_final.F90: elastic Rayleigh eqn. analog to shoot2abs_final

mean_flow.F90: Compute base flow for linear stability equations. See subroutine mean_flow
  for different base flows available (specified with input variable mean_type)

modules.F90: Various variables

orthogonalize.F90: Used to orthogonalize two independent vectors, called by
  OS_integrate, and calculate_eigenvectors

polymer_mean.F90: Set of subroutines used to compute base-state polymer stress/conformation
  tensor using FENE-P, Oldroyd_B, or Giesekus models as called by polymer_mean

polymer_model.F90: constitutive models called by Derivs subroutine in main code
  giesekus_model2 and FENE_model are alternate, possibly equivalent formulations
  to the main subroutines giesekeus_model and  FENE_model2
  The subroutines assemble the vectors R and S defined in Ray & Zaki (2014)

shoot2abs_final.F90: Viscoelastic spatiotemporal linear stability solver: find saddle point with S specified

shoot2absf_final.F90: Viscoelastic spatiotemporal linear stability solver: find critical S (Scrit) for onset
  of absolute stability (cf. Ray & Zaki 2015)

shoot2w_final.F90: Viscoelastic spatial linear stability solver: find spatial growth rate for
  range of complex frequencies

shoot2w_finalt.F90: Viscoelastic temporal linear stability solver: find temporal growth rate for
  range of wavenumbers
